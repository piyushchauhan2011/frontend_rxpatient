'use strict';

angular.module('frontendRxpatientApp', [
  'ngCookies',
  'ngResource',
  'ngSanitize',
  'ngRoute'
  // 'ui.router'
])
 // .config(function($stateProvider, $urlRouterProvider) {
 //    $urlRouterProvider.otherwise('/');

 //    $stateProvider
 //     .state('root', {
 //        url: '/',
 //        templateUrl: 'views/main.html',
 //        controller: 'MainCtrl'
 //     })
 //     .state('/index', {
 //        url: '/index',
 //        templateUrl: 'views/index.html',
 //        controller: 'IndexCtrl'
 //     })
 //     .state('profile', {
 //        url: '/profile',
 //        templateUrl: 'views/profile.html',
 //        controller: 'ProfileCtrl'
 //     })
 //     .state('signup', {
 //        url: '/signup',
 //        templateUrl: 'views/signup.html',
 //        controller: 'SignupCtrl'
 //     })
 //     .state('appointmentIndex', {
 //        url: '/appointment',
 //        templateUrl: 'views/appointment_index.html',
 //        controller: 'AppointmentIndexCtrl'
 //     })
 //     .state('appointmentBook', {
 //        url: '/appointment/book',
 //        templateUrl: 'views/appointment_book.html',
 //        controller: 'AppointmentBookCtrl'
 //     });
 // })
  .config(function($routeProvider) {
    $routeProvider
     .when('/', {
        templateUrl: 'views/main.html',
        controller: 'MainCtrl'
     })
     .when('/signin', {
        templateUrl: 'views/main.html',
        controller: 'MainCtrl'
     })
     .when('/index', {
        templateUrl: 'views/index.html',
        controller: 'IndexCtrl'
     })
     .when('/features', {
        templateUrl: 'views/about.html',
        controller: 'AboutCtrl'
     })
     .when('/keywords', {
        templateUrl: 'views/keywords.html',
        controller: 'KeywordsCtrl'
     })
     .when('/profile', {
        templateUrl: 'views/profile.html',
        controller: 'ProfileCtrl'
     })
     .when('/signup', {
        templateUrl: 'views/signup.html',
        controller: 'SignupCtrl'
     })
     .when('/appointment', {
        templateUrl: 'views/appointment_index.html',
        controller: 'AppointmentIndexCtrl'
     })
     .when('/appointment/book', {
        templateUrl: 'views/appointment_book.html',
        controller: 'AppointmentBookCtrl'
     })
     .when('/prescriptions', {
        templateUrl: 'views/prescriptions.html',
        controller: 'PrescriptionsCtrl'
     })
     .when('/prescriptions/new', {
        templateUrl: 'views/prescriptions_new.html',
        controller: 'PrescriptionsNewCtrl'
     })
     // .when('/prescriptions/edit/:id', {
     //    templateUrl: 'views/prescriptions_edit.html',
     //    controller: 'PrescriptionsEditCtrl'
     // })
     .when('/saved_articles', {
        templateUrl: 'views/saved_articles.html',
        controller: 'SavedArticlesCtrl'
     })
     .when('/prescriptions/view/:appointmentId/edit/:medicationId', {
        templateUrl: 'views/medication_edit.html',
        controller: 'MedicationEditCtrl'
     })
     .when('/prescriptions/view/:appointmentId/new', {
        templateUrl: 'views/medication_new.html',
        controller: 'MedicationNewCtrl'
     })
     .when('/prescriptions/view/:appointmentId', {
        templateUrl: 'views/prescriptions_view.html',
        controller: 'PrescriptionsViewCtrl'
     })
     .when('/allergies', {
        templateUrl: 'views/allergies.html',
        controller: 'AllergiesCtrl'
     })
     .when('/allergies/edit/:id', {
        templateUrl: 'views/allergies_edit.html',
        controller: 'AllergiesEditCtrl'
     })
     .when('/allergies/new', {
        templateUrl: 'views/allergies_new.html',
        controller: 'AllergiesNewCtrl'
     })
     .when('/immunizations', {
        templateUrl: 'views/immunizations.html',
        controller: 'ImmunizationsCtrl'
     })
     .when('/immunizations/edit/:id', {
        templateUrl: 'views/immunizations_edit.html',
        controller: 'ImmunizationsEditCtrl'
     })
     .when('/immunizations/new', {
        templateUrl: 'views/immunizations_new.html',
        controller: 'ImmunizationsNewCtrl'
     })
     .when('/obstetrics', {
        templateUrl: 'views/obstetrics.html',
        controller: 'ObstetricsCtrl'
     })
     .when('/obstetrics/edit/:id', {
        templateUrl: 'views/obstetrics_edit.html',
        controller: 'ObstetricsEditCtrl'
     })
     .when('/obstetrics/new', {
        templateUrl: 'views/obstetrics_new.html',
        controller: 'ObstetricsNewCtrl'
     })
     .when('/conditions', {
        templateUrl: 'views/conditions.html',
        controller: 'ConditionsCtrl'
     })
     .when('/conditions/edit/:id', {
        templateUrl: 'views/conditions_edit.html',
        controller: 'ConditionsEditCtrl'
     })
     .when('/conditions/new', {
        templateUrl: 'views/conditions_new.html',
        controller: 'ConditionsNewCtrl'
     })
     .when('/surgeries', {
        templateUrl: 'views/surgeries/index.html',
        controller: 'SurgeriesIndexCtrl'
     })
     .when('/surgeries/edit/:id', {
        templateUrl: 'views/surgeries/edit.html',
        controller: 'SurgeriesEditCtrl'
     })
     .when('/surgeries/new', {
        templateUrl: 'views/surgeries/new.html',
        controller: 'SurgeriesNewCtrl'
     })
     .when('/emergency_contacts', {
        templateUrl: 'views/emergency_contacts/index.html',
        controller: 'EmergencyContactsIndexCtrl'
     })
     .when('/emergency_contacts/edit/:id', {
        templateUrl: 'views/emergency_contacts/edit.html',
        controller: 'EmergencyContactsEditCtrl'
     })
     .when('/emergency_contacts/new', {
        templateUrl: 'views/emergency_contacts/new.html',
        controller: 'EmergencyContactsNewCtrl'
     })
     .when('/insurances', {
        templateUrl: 'views/insurances/index.html',
        controller: 'InsurancesIndexCtrl'
     })
     .when('/insurances/edit/:id', {
        templateUrl: 'views/insurances/edit.html',
        controller: 'InsurancesEditCtrl'
     })
     .when('/insurances/new', {
        templateUrl: 'views/insurances/new.html',
        controller: 'InsurancesNewCtrl'
     })
     // .when('/families', {
     //    templateUrl: 'views/families.html',
     //    controller: 'FamiliesCtrl'
     // })
     .when('/info', {
        templateUrl: 'views/basicinfo.html',
        controller: 'BasicinfoCtrl'
     })
     .when('/vitals', {
        templateUrl: 'views/vitals.html',
        controller: 'VitalsCtrl'
      })
     .when('/queries', {
        templateUrl: 'views/queries.html',
        controller: 'QueriesCtrl'
      })
      .when('/feedback', {
        templateUrl: 'views/feedback.html',
        controller: 'FeedbackCtrl'
      })
      .when('/healthlogs', {
        templateUrl: 'views/healthlogs.html',
        controller: 'HealthlogsCtrl'
      })
      .when('/nearby', {
        templateUrl: 'views/nearby.html',
        controller: 'NearbyCtrl'
      })
     .otherwise({
        redirectTo: '/'
      });
 })
.run(['$rootScope', '$window', '$cookieStore', function($rootScope, $window, $cookieStore) {
  var jQuery = $window.jQuery;
  $rootScope.mapLoaded = false;
  $rootScope.isUserLoggedIn = false;
  var patientToken = $cookieStore.get('patientToken');
  $rootScope.patient = $cookieStore.get('patient');
  // console.log(patientToken);
  // var patientId = $cookieStore.get('patientId');
  $rootScope.closeErrors = function() {
    $rootScope.bErrors = false;
  };
  // $rootScope.$on('$locationChangeStart', function() {
  //   angular.element('body').scrollTop(0);
  // });
  $rootScope.$on('$routeChangeStart', function() { // function(e,curr,prev)
    $rootScope.topBarShow = true;
    $rootScope.subNavigationShow = false;
    $rootScope.transparentTopBarShow = false;
    $rootScope.patient = $cookieStore.get('patient');
    if(patientToken) {
      $rootScope.isUserLoggedIn = true;
    }
    jQuery('#rx_hoax_margin').show();
    jQuery('.rx-pdtb').show();
    // Below statement is used to scroll to top when route changes because of issue that
    // page is retaining its current scollTop position between pages
    // A hack to make user appear that he is reading from top
    angular.element('html,body').animate({scrollTop: 0}, 'fast');
    var hoaxMarginEl = jQuery('#rx_hoax_margin');
    hoaxMarginEl.css('height', '2em');
    
    // Sub Navigation Variables
    $rootScope.subNavigationIndex = 'inactive';
    $rootScope.subNavigationProfile = 'inactive';
    $rootScope.subNavigationSavedArticles = 'inactive';
    $rootScope.subNavigationHealthlogs = 'inactive';

    // Common Functionalities
    // console.log('This is triggered!');
    $rootScope.toBeShown = false;
    $rootScope.preloading = true;
    jQuery('.map-hide').css('display', 'none');
    hoaxMarginEl.show();
    jQuery('#map_canvas').css('z-index', -1);
    jQuery('#map_canvas').css('height', '20em');
    jQuery('#footer').show();
    // if(patientToken == null) {
    //   $location.path('/');
    // }
  });
  $rootScope.$on('$routeChangeSuccess', function() {
    // console.log('Success Changing the route.');
    jQuery('footer').css('display', 'block');
    $rootScope.preloading = false;
  });
  jQuery('#hoax').css('display', 'block');

  // Local Api URLs Global Variables ( comment while deploying )
  $rootScope.patientApiUrl = 'http://localhost:4000';
  $rootScope.doctorApiUrl = 'http://localhost:3000';
  $rootScope.appointmentApiUrl = 'http://localhost:5000';
  $rootScope.articlesApiUrl = 'http://localhost:7000';
  $rootScope.locationApiUrl = 'http://localhost:8080';
  $rootScope.feedbackApiUrl = 'http://localhost:1337';

  $rootScope.googleMapsUrl = 'https://maps.googleapis.com/maps/api/js?key=AIzaSyDvncyrGON3jeOV1bCmI_DiDZlApRde1d0&sensor=false&libraries=drawing,places&callback=initialize';

  // Server Api URLs Global Variables ( Uncomment while deploying and run grunt )
  // $rootScope.patientApiUrl = 'api/patient_rxhealth';
  // $rootScope.doctorApiUrl = 'api/rxhealth_doctor';
  // $rootScope.appointmentApiUrl = 'api/rxhealth_al';
  // $rootScope.articlesApiUrl = 'api/articles_rxhealth';
  // $rootScope.locationApiUrl = 'api/location_rxhealth';
  // $rootScope.feedbackApiUrl = 'api/feedback_rxhealth';
}]);
 /*.config(function ($routeProvider) {
    $routeProvider
      .when('/', {
        templateUrl: 'views/main.html',
        controller: 'MainCtrl'
      })
      .when('/conditions', {
        templateUrl: 'views/conditions.html',
        controller: 'ConditionsCtrl'
      })
      .when('/families', {
        templateUrl: 'views/families.html',
        controller: 'FamiliesCtrl'
      })
      .when('/basicInfo', {
        templateUrl: 'views/basicinfo.html',
        controller: 'BasicinfoCtrl'
      })
      .when('/vitals', {
        templateUrl: 'views/vitals.html',
        controller: 'VitalsCtrl'
      })
      .when('/queries', {
        templateUrl: 'views/queries.html',
        controller: 'QueriesCtrl'
      })
      .when('/feedback', {
        templateUrl: 'views/feedback.html',
        controller: 'FeedbackCtrl'
      })
      .when('/healthlogs', {
        templateUrl: 'views/healthlogs.html',
        controller: 'HealthlogsCtrl'
      })
      .when('/nearby', {
        templateUrl: 'views/nearby.html',
        controller: 'NearbyCtrl'
      })
      .otherwise({
        redirectTo: '/'
      });
  });*/
