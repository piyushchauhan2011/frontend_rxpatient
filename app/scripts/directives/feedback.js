'use strict';

angular.module('frontendRxpatientApp')
  .directive('feedback',
    function() {
      return {
        link: function(scope, element) {
          // Feedback JS
          // console.log(scope);
          // console.log(element);

          var rxFarray = [];
          var counter = 0;
          var rxF1 = element.find('.rx-f1'); rxFarray.push(rxF1);
          var rxF2 = element.find('.rx-f2'); rxFarray.push(rxF2);
          var rxF3 = element.find('.rx-f3'); rxFarray.push(rxF3);
          var rxF4 = element.find('.rx-f4'); rxFarray.push(rxF4);
          var rxF5 = element.find('.rx-f5'); rxFarray.push(rxF5);

          var rxFs = element.find('.rx-f');

          // Left and Right Movement Buttons
          var lButton = element.find('.rx-fal');
          var rButton = element.find('.rx-far');

          lButton.click(function() {
            counter--;
            if(counter === -1) { counter++; }
            else {
              rxFs.hide();
              rxFarray[counter].slideDown('fast');
            }
          });
          rButton.click(function() {
            counter++;
            if(counter === 5) { counter--; }
            else {
              rxFs.hide();
              rxFarray[counter].slideDown('fast');
            }
          });

          // First Box JS
          var rxFbox = element.find('.rx-fbox');
          var fIcon = element.find('.ficon');
          var fSaying = element.find('.fsaying');
          fIcon.addClass('start');
          rxF1.css('background', '#352222');
          fSaying.html('Start Over!');
          rxFbox.hover(function() {
            fIcon.removeClass('start');
            var el = angular.element(this);
            var color = el.css('background-color');
            if(el.hasClass('rx-fb1')) {
              fIcon.css('background-position', '0px 0px');
              fSaying.html('Hell No!');
            } else if(el.hasClass('rx-fb2')) {
              fIcon.css('background-position', '-58px 0px');
              fSaying.html('I won\'t!');
            } else if(el.hasClass('rx-fb3')) {
              fIcon.css('background-position', '-116px 0px');
              fSaying.html('Not Yet.');
            } else if(el.hasClass('rx-fb4')) {
              fIcon.css('background-position', '-174px 0px');
              fSaying.html('Maybe.');
            } else if(el.hasClass('rx-fb5')) {
              fIcon.css('background-position', '-232px 0px');
              fSaying.html('Hmmm..');
            } else if(el.hasClass('rx-fb6')) {
              fIcon.css('background-position', '-289px 0px');
              fSaying.html('Fine.');
            } else if(el.hasClass('rx-fb7')) {
              fIcon.css('background-position', '-347px 0px');
              fSaying.html('Okay!');
            } else if(el.hasClass('rx-fb8')) {
              fIcon.css('background-position', '-405px 0px');
              fSaying.html('Yes, I will.');
            } else if(el.hasClass('rx-fb9')) {
              fIcon.css('background-position', '-463px 0px');
              fSaying.html('Definitely.');
            } else if(el.hasClass('rx-fb10')) {
              fIcon.css('background-position', '-521px 0px');
              fSaying.html('I already have!');
            }
            rxF1.css('background', color);
          });

          // Page 1 Clickable features
          var rxFbArray = [];
          var i;
          for(i=1;i<=10;i++) {
            rxFbArray[i] = element.find('.rx-fb'+i);
          }
          var removeActiveClass = function() {
            for(i=1;i<=10;i++) {
              rxFbArray[i].removeClass('active');
            }
          };
          var rxFbAddActive = function(counter) {
            rxFbArray[counter].click(function() {
              removeActiveClass();
              angular.element(this).addClass('active');
            });
          };
          for(i=1;i<=10;i++) {
            rxFbAddActive(i);
          }

          // Page 3 Clickable features
          var usabilityArray = [];
          usabilityArray[0] = element.find('.hard');
          usabilityArray[1] = element.find('.okay');
          usabilityArray[2] = element.find('.easy');
          var removeActiveClassU = function() {
            for(i=0;i<3;i++) {
              usabilityArray[i].removeClass('active');
            }
          };
          var usabilityAddActive = function(counter) {
            usabilityArray[counter].click(function() {
              removeActiveClassU();
              angular.element(this).addClass('active');
            });
          };
          for(i=0;i<3;i++) {
            usabilityAddActive(i);
          }
        }
      };
    }
);