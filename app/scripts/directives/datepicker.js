'use strict';

angular.module('frontendRxpatientApp')
  .directive('datepicker', ['$window', function ($window) {
    return {
      template: '<input />',
      restrict: 'AE',
      scope: {
        date: '='
      },
      replace: true,
      link: function postLink(scope, element) {
        var jQuery = $window.jQuery;
        var el = jQuery(element);
        var birthdayVal;
        
        el.datepicker({
          changeMonth: true,
          changeYear: true,
          gotoCurrent: true,
          dateFormat: 'MM dd, yy',
          minDate: '-110Y',
          maxDate: '0Y',
          yearRange: '-100:+0',
          onSelect: function(date) {
            scope.date = date;
            scope.$apply();
            el.trigger('blur');
          }
        });
        el.datepicker('option', 'showAnim', 'fade');
        // el.bdatepicker();

        birthdayVal = scope.date;
        el.datepicker('setDate', new Date(birthdayVal));
        scope.$watch('date', function(date) {
          birthdayVal = date;
          el.datepicker('setDate', new Date(birthdayVal));
        });

        jQuery('.ui-datepicker').wrap('<div id=' + 'rx_gdp></div>');
      }
    };
  }]);
