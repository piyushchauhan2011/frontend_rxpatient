'use strict';

angular.module('frontendRxpatientApp')
  .directive('fixSideNav', ['$window',
    function($window) {
      return {
        link: function(scope,element) {
          var sideNav = element.find('.rx-pp-sn-wrap');
          var fixedSideNav = element.find('.rx-pp-sn-wrap-fixed');
          var angularWindow = angular.element($window);
          angularWindow.scroll(function() {
            var scrollTop = angularWindow.scrollTop();
            if (scrollTop > 200) {
              fixedSideNav.show();
              sideNav.css('opacity',0);
            } else {
              fixedSideNav.hide();
              sideNav.css('opacity',1);
            }
          });
        }
      };
    }
  ]);