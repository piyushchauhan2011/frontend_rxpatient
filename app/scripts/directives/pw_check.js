'use strict';

angular.module('frontendRxpatientApp')
  .directive('pwCheck', ['$window', function ($window) {
  return {
    require: 'ngModel',
    link: function (scope, elem, attrs, ctrl) {
      var jQuery = $window.jQuery;
      var firstPassword = 'input[name=' + '\'' + attrs.pwCheck + '\']';
      jQuery(elem).add(firstPassword).on('keyup', function () {
        scope.$apply(function () {
          var v = jQuery(elem).val()===jQuery(firstPassword).val();
          ctrl.$setValidity('pwmatch', v);
        });
      });
    }
  };
}]);