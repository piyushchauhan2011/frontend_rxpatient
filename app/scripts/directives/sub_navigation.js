'use strict';

angular.module('frontendRxpatientApp')
  .directive('subnavigation', ['$window',
    function($window) {
      return {
        link: function(scope, element) {
          // Sub Navigation Sticky JS
          var subNavigation = angular.element('#rx_pp_sub_nav');
          var profilePicEl = angular.element('#rx_pp_pi_img');
          var articlesSearchEl = angular.element('#rx_pp_articles_search');
          var hospitalsSearchEl = angular.element('#rx_pp_hospitals_search');
          var choicesDropDown = angular.element('#rx_pp_stype');
          var patientInfoBar = angular.element('#rx_pp_info_bar');
          var horizontalSearchChoices = angular.element('#rx_pp_hschoices');
          // var searchValue = angular.element('#rx_pp_stype .sd-current').text().trim();
          // var stickySubNavTop = subNavigation.offset().top;
          // console.log($window.scroll);
          var angularWindow = angular.element($window);
          angularWindow.scroll(function() {
            var scrollTop = angularWindow.scrollTop();
            if (scrollTop > 200) {
              subNavigation.addClass('sub-nav-sticky');
              profilePicEl.css('display', 'none');
              choicesDropDown.css('display', 'none');
              patientInfoBar.css('display', 'none');
              horizontalSearchChoices.css('display', 'none');
              
              // Bad Code: Use to show which one was active before scrolling and after scrolling
              // Uses dropdownCurrentSelection Variable to get info about which item is selected in horizontal or drop down menu
              // Display none code
              if(scope.dropdownCurrentSelection === 'Articles') {
                articlesSearchEl.css('display', 'none');
              } else {
                hospitalsSearchEl.css('display', 'none');
              }
            } else {
              subNavigation.removeClass('sub-nav-sticky');
              profilePicEl.css('display', 'block');
              choicesDropDown.css('display', 'block');
              patientInfoBar.css('display', 'block');
              horizontalSearchChoices.css('display', 'block');
              
              // Replication of above code for displaying it as block.
              if(scope.dropdownCurrentSelection === 'Articles') {
                articlesSearchEl.css('display', 'block');
              } else {
                hospitalsSearchEl.css('display', 'block');
              }
            }
          });

          // Image Lazy Loads
          var imageTag = element.find('img');
          imageTag
          .load(function() {
            // console.log('image loaded correctly');
            imageTag.css('display', 'block');
          })
          .error(function() {
            // console.log('error loading image');
            imageTag.attr('src', 'https://s3-ap-southeast-1.amazonaws.com/rxhealth-images/150x150.gif');
          });
        }
      };
    }
  ]);
