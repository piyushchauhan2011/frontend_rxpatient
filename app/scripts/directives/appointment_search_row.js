'use strict';

angular.module('frontendRxpatientApp')
  .directive('appointmentSearchRow', ['$window', function ($window) {
    return {
      templateUrl: 'views/_appointment_search_row.html',
      restrict: 'AE',
      link: function postLink(scope, element) {
        var jQuery = $window.jQuery;
        var el = jQuery(element);
        var bab = el.find('.rx-appsea-rb-bab');
        bab.click(function() {
          el.find('.rx-appsea-rb-d').slideToggle('fast');
          var button = bab.find('button');
          if(!button.hasClass('active')) {
            button.addClass('active');
          } else {
            button.removeClass('active');
          }
        });
      }
    };
  }]);
