'use strict';

angular.module('frontendRxpatientApp')
  .directive('vitalChart', ['$window', function(window) {
    return {
      restrict: 'AE',
      link: function postLink(scope, element, attrs) {
        scope.$watch(attrs.data, function(value) {
          // console.log('PROMISE: ' + value);
          try {
          var google = window.google;
          var data = new google.visualization.DataTable();
          // data.addColumn('string', 'Vital');
          data.addColumn('number', 'weight');
          data.addColumn('number', 'systolic_bp');
          data.addColumn('number', 'diastolic_bp');
          data.addColumn('number', 'pulse_rate');
          data.addColumn('number', 'temperature');
          data.addColumn('number', 'respiration_rate');
          data.addColumn('number', 'height');

          angular.forEach(value, function(row) {
            data.addRow([row.weight, row.systolic_bp, row.diastolic_bp, row.pulse_rate, row.temperature, row.respiration_rate, row.height]);
          });

          var options = {
            title: attrs.title,
            height: attrs.height,
            width: attrs.width,
            legend: 'bottom',
            fontName: 'Trebuchet MS',
            fontSize: 13,
            backgroundColor: '#FAFAFA',
            curveType: 'function'
          };

          //render the desired chart based on the type attribute provided
          var chart = new google.visualization.ColumnChart(element[0]);
          chart.draw(data, options);
          } catch(e) {}
        });
      }
    };
  }]);
