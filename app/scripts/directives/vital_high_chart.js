'use strict';

angular.module('frontendRxpatientApp')
  .directive('vitalHighChart', function() {
    return {
      restrict: 'AE',
      link: function postLink(scope, element, attrs) {
        scope.$watch(attrs.data, function() {
          // console.log('PROMISE: ' + value);
          try {
            element.highcharts({
              chart: {
                  type: 'column',
                  backgroundColor: '#FCFCFC'
              },
              title: {
                  text: 'Fruit Consumption'
              },
              xAxis: {
                  categories: ['Apples', 'Bananas', 'Oranges']
              },
              yAxis: {
                  title: {
                      text: 'Fruit eaten'
                  }
              },
              series: [{
                  name: 'Jane',
                  data: [1, 0, 4]
              }, {
                  name: 'John',
                  data: [5, 7, 3]
              }]
            });
          } catch(e) {}
        });
      }
    };
  });
