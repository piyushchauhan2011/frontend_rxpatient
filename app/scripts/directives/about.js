'use strict';

angular.module('frontendRxpatientApp')
  .directive('about', ['$window',
    function($window) {
      return {
        link: function() {
          // About Us Directive JS
          var $ = $window.jQuery;
          $('#rx_hoax_margin').hide();
          $('.rx-pdtb').hide();
          $('section[data-type="background"]').each(function() {
            var $bgobj = $(this); // assigning the object

            $(window).scroll(function() {
              var yPos = -($(window).scrollTop() / $bgobj.data('speed'));
              // Put together our final background position
              var coords = '50% ' + yPos + 'px';
              // Move the background
              $bgobj.css({
                backgroundPosition: coords
              });
            });
          });
        }
      };
    }
  ]);
