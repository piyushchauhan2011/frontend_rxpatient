'use strict';

angular.module('frontendRxpatientApp')
  .directive('choicesDropdown', ['$document',
    function($document) {
      return {
        link: function(scope, element) {
          // Choices DropDown JS
          // var choicesDropdown = element.find('.stype-dropdown');
          var choicesItem = element.find('.sd-item-wrap');
          var choicesCurrent = element.find('.sd-current');
          var hospitalsSearchInput = angular.element('#rx_pp_hospitals_search');
          var articlesSearchInput = angular.element('#rx_pp_search');
          var searchResults = angular.element('.search-results');
          choicesCurrent.click(function() {
            choicesItem.slideToggle('fast');
          });
          scope.dropdownCurrentSelection = 'Articles';
          scope.dropdownValues = ['Articles', 'Hospitals'];
          element.bind('click', function(e) {
            e.stopPropagation();
          });
          $document.bind('click', function() {
            choicesItem.slideUp('fast');
          });
          scope.setDropdownCurrentSelection = function(value) {
            scope.dropdownCurrentSelection = value;
            choicesItem.slideToggle('fast');
            if (value === 'Hospitals') {
              hospitalsSearchInput.show();
              hospitalsSearchInput.find('input').focus();
              articlesSearchInput.hide();
              searchResults.hide();
            } else {
              hospitalsSearchInput.hide();
              articlesSearchInput.show();
              searchResults.show();
              scope.subMenuStatus = true;
              articlesSearchInput.find('input').focus();
            }
          };
        }
      };
    }]
);
