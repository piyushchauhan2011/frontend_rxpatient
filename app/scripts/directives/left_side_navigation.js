'use strict';

angular.module('frontendRxpatientApp')
  .directive('leftSideNavigation', ['$window',
    function($window) {
      return {
        link: function() {
          // Sub Navigation Sticky JS
          var leftSideNavigationHidden = angular.element('#rx_ppls_hidden');
          var leftSideNavigation = angular.element('#rx_ppls');
          // var mainContent = angular.element('#rx_pplc');
          // var stickySideNavTop = leftSideNavigation.offset().top;
          // console.log($window.scroll);
          var angularWindow = angular.element($window);
          angularWindow.scroll(function() {
            var scrollTop = angularWindow.scrollTop();
            if (scrollTop > 200) {
              // leftSideNavigation.addClass('left-side-nav-sticky');
              leftSideNavigationHidden.show();
              leftSideNavigation.css('opacity', 0);
              // mainContent.css('margin-left', '25%');
            } else {
              // leftSideNavigation.removeClass('left-side-nav-sticky');
              leftSideNavigationHidden.hide();
              leftSideNavigation.css('opacity', 1);
              // mainContent.css('margin-left', '0');
            }
          });
        }
      };
    }
  ]);
