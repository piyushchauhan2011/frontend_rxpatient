'use strict';

angular.module('frontendRxpatientApp')
  .factory('Doctors', ['$resource', '$rootScope', function ($resource, $rootScope) {
    return $resource($rootScope.doctorApiUrl+'/doctors/:id?token=:token', {
      Id: '@Id', token: '@token'}, {
        update: {method: 'PUT'}
    });
  }]);
