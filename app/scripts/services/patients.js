'use strict';

angular.module('frontendRxpatientApp')
  .factory('Patients', ['$resource', '$rootScope', function ($resource, $rootScope) {
    return $resource($rootScope.patientApiUrl+'/patients/:id?token=:token', {
      id: '@Id', token: '@token'}, {
        update: {method: 'PUT'}
    });
  }]);
