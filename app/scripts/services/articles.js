'use strict';

angular.module('frontendRxpatientApp')
  .factory('Articles', ['$resource', '$rootScope', function ($resource, $rootScope) {
    // Service logic
    return $resource($rootScope.articlesApiUrl+'/articles/:id'+'.json?token=:token', {
      id: '@Id', token: '@token'}, {
        update: {method: 'PUT'}
    });
  }]);
