'use strict';

angular.module('frontendRxpatientApp')
  .factory('Conditions', ['$resource', '$rootScope', function ($resource, $rootScope) {
    return $resource($rootScope.patientApiUrl+'/conditions/:id?token=:token', {
      id: '@Id', token: '@token'}, {
        update: {method: 'PUT'}
    });
  }]);
