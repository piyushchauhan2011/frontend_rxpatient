'use strict';

angular.module('frontendRxpatientApp')
  .factory('Vitals', ['$resource', '$rootScope', function ($resource, $rootScope) {
    return $resource($rootScope.patientApiUrl+'/vitals/:id?token=:token', {
      Id: '@Id', token: '@token'}, {
        update: {method: 'PUT'}
    });
  }]);
