'use strict';

angular.module('frontendRxpatientApp')
  .factory('Allergies', ['$resource', '$rootScope', function ($resource, $rootScope) {
    return $resource($rootScope.patientApiUrl+'/allergies/:id?token=:token', {
      id: '@Id', token: '@token'}, {
        update: {method: 'PUT'}
    });
  }]);
