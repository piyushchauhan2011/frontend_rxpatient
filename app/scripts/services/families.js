'use strict';

angular.module('frontendRxpatientApp')
  .factory('Families', ['$resource', '$rootScope', function ($resource, $rootScope) {
    // Service logic
    return $resource($rootScope.patientApiUrl+'/families/:id?token=:token', {
      Id: '@Id', token: '@token'}, {
        update: {method: 'PUT'}
    });
  }]);
