'use strict';

angular.module('frontendRxpatientApp')
  .factory('EmergencyContacts', ['$resource', '$rootScope', function ($resource, $rootScope) {
    return $resource($rootScope.patientApiUrl+'/emergency_contacts/:id?token=:token', {
      id: '@Id', token: '@token'}, {
        update: {method: 'PUT'}
    });
  }]);
