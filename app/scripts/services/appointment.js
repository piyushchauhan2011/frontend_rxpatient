'use strict';

angular.module('frontendRxpatientApp')
  .factory('Appointment', ['$resource', '$rootScope', function ($resource, $rootScope) {
    // Service logic
    return $resource($rootScope.appointmentApiUrl+'/appointments/:id?token=:token', {
      Id: '@Id', token: '@token'}, {
        update: {method: 'PUT'}
    });
  }]);
