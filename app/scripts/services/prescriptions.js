'use strict';

angular.module('frontendRxpatientApp')
  .factory('Prescriptions', ['$resource', '$rootScope', function ($resource, $rootScope) {
    // Service logic
    return $resource($rootScope.patientApiUrl+'/prescriptions/:id?token=:token', {
      id: '@Id', token: '@token'}, {
        update: {method: 'PUT'}
    });
  }]);
