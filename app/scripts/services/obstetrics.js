'use strict';

angular.module('frontendRxpatientApp')
  .factory('Obstetrics', ['$resource', '$rootScope', function ($resource, $rootScope) {
    return $resource($rootScope.patientApiUrl+'/obstetrics_histories/:id?token=:token', {
      id: '@Id', token: '@token'}, {
        update: {method: 'PUT'}
    });
  }]);
