'use strict';

angular.module('frontendRxpatientApp')
  .factory('Map', ['$window', '$rootScope',
    function($window, $rootScope) {
      var map_canvas = document.getElementById('map_canvas');

      function initialize() {
        var google = $window.google;
        $rootScope.google = google;
        if (!$rootScope.mapLoaded) {
          if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(function(position) {
              // console.log(position.coords.latitude);
              // console.log(position.coords.longitude);
              var map_options = {
                center: new google.maps.LatLng(position.coords.latitude, position.coords.longitude),
                zoom: 5,
                mapTypeId: google.maps.MapTypeId.ROADMAP,
                scrollwheel: false
              };
              new google.maps.Map(map_canvas, map_options);
              google.maps.event.trigger(map_canvas, 'resize');
            });
          } else {
            console.log('Geolocation is not supported by your browser');
            var map_options = {
              center: new google.maps.LatLng(21.1289955, 82.7792201),
              zoom: 5,
              mapTypeId: google.maps.MapTypeId.ROADMAP,
              scrollwheel: false
            };
            new google.maps.Map(map_canvas, map_options);
          }
          $rootScope.mapLoaded = true;
        }
      }
      return {
        mapLoad: function() {
          // head.load($rootScope.googleMapsUrl, function() {
          initialize();
          console.log($window.google.maps.event.trigger);
          // });
        }
      };
    }
  ]);
