'use strict';

angular.module('frontendRxpatientApp')
  .factory('Surgeries', ['$resource', '$rootScope', function ($resource, $rootScope) {
    return $resource($rootScope.patientApiUrl+'/surgeries/:id?token=:token', {
      id: '@Id', token: '@token'}, {
        update: {method: 'PUT'}
    });
  }]);
