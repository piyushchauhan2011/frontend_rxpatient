'use strict';

angular.module('frontendRxpatientApp')
  .factory('Immunizations', ['$resource', '$rootScope', function ($resource, $rootScope) {
    return $resource($rootScope.patientApiUrl+'/immunization_histories/:id?token=:token', {
      id: '@Id', token: '@token'}, {
        update: {method: 'PUT'}
    });
  }]);
