'use strict';

angular.module('frontendRxpatientApp')
  .controller('KeywordsCtrl', ['$scope', '$window', '$rootScope', '$cookieStore', '$http',
    function($scope, $window, $rootScope, $cookieStore, $http) {

      var jQuery = $window.jQuery;
      var patientToken = $cookieStore.get('patientToken');
      var patientId = $cookieStore.get('patientId');
      // $scope.keywords = [
      //   'Arthritis',
      //   'Asthma',
      //   'Back and Neck Pain',
      //   'Cancer Treatment, ..',
      //   'Depression',
      //   'Fevers',
      //   'Heart Attack',
      //   'Heartburn',
      //   'Hepatitis A',
      //   'HIV and AIDS',
      //   'Kidney Stones',
      //   'Migraine Headache',
      //   'Nosebleeds',
      //   'Sinusitis',
      //   'Stroke',
      //   'Urinary Tract Infection'
      // ];
      // $scope.keywordsImages = ['arthritis.jpg', 'asthma.jpg', 'back_and_pain.jpg'];
      var userKeywords = [];
      var keywordItem = angular.element('.rx-skc-item');
      keywordItem.click(function() {
        angular.element(this).toggleClass('active');
      });
      $scope.toggleKeyword = function(id, value) {
        if(userKeywords[id]===null || userKeywords[id]===undefined) {
          userKeywords[id] = value;
        } else {
          userKeywords[id] = null;
        }
      };
      $scope.proceedWithSelection = function() {
        var tmp = jQuery.grep(userKeywords, function(n) { return n; });
        // console.log(tmp);
        $http.put($rootScope.patientApiUrl+'/patients/'+patientId+'?token='+patientToken, { patient: { article_keywords: tmp } })
        .success(function() {
          // console.log(data);
        }).error(function() { });
      };
    }
  ]);
