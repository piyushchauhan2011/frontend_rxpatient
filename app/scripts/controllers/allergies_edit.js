'use strict';

angular.module('frontendRxpatientApp')
  .controller('AllergiesEditCtrl', ['$scope', '$rootScope', '$window', '$cookieStore', '$routeParams', '$http', 'Allergies',
    function($scope, $rootScope, $window, $cookieStore, $routeParams, $http, Allergies) {
      // var jQuery = $window.jQuery;
      // var allergyId = '9a61be15-8e60-482d-9f1c-1accacdb00c9';
      var allergyId = $routeParams.id;
      $scope.allergyId = allergyId;
      $scope.activeAllergies = 'rx-active';
      $rootScope.toBeShown = true;
      $rootScope.subNavigationShow = true;

      var patientToken = $cookieStore.get('patientToken');
      // var patientId = $cookieStore.get('patientId');

      Allergies.get({
        id: allergyId,
        token: patientToken
      }, function(allergy) {
        $scope.allergy = allergy;
      });

      $scope.updateAllergy = function() {
        $scope.isDisabled = true;
        delete $scope.allergy.created_at;
        delete $scope.allergy.updated_at;

        Allergies.update({
          Id: allergyId,
          token: patientToken,
          allergy: $scope.allergy
        }, function() {
          // console.log(message);
          $scope.isDisabled = false;
        }, function() {
          // console.log(err);
          $scope.isDisabled = false;
        });
      };
    }
  ]);
