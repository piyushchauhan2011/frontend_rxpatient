'use strict';

angular.module('frontendRxpatientApp')
  .controller('BasicinfoCtrl', ['$scope', '$window', '$rootScope', '$cookieStore', 'Patients', function ($scope, $window, $rootScope, $cookieStore, Patients) {
    var jQuery = $window.jQuery;

    $rootScope.toBeShown = true;
    $rootScope.subNavigationShow = true;
    $scope.activeBasic = 'rx-active';

    var patientToken = $cookieStore.get('patientToken');
    var patientId = $cookieStore.get('patientId');
    var patient = $cookieStore.get('patient');

    // $scope.patient = Patients.get({
    //   id: patientId,
    //   token: patientToken
    // }, function() {
    //   // console.log($scope.patient);
    // });
    $scope.patient = patient;

    $scope.bloodGroups = ['O-', 'O+', 'A-', 'A+', 'B-', 'B+', 'AB-', 'AB+'];
    var rxSRadio = jQuery('.rx-sradio');
    $scope.addFocusClass = function() {
      if(!rxSRadio.hasClass('focus')) {
        rxSRadio.addClass('focus');
      }
    };
    $scope.removeFocusClass = function() {
      rxSRadio.removeClass('focus');
    };

    $scope.updateInfo = function() {
      // console.log($scope.patient);
      Patients.update({
        Id: patientId,
        token: patientToken,
        patient: $scope.patient
      }, function() {
        // console.log(message);
      });
    };

  }]);
