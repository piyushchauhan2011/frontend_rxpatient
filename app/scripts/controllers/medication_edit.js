'use strict';

angular.module('frontendRxpatientApp')
  .controller('MedicationEditCtrl', ['$scope', '$rootScope', '$window', '$location', '$cookieStore', '$routeParams', '$http', 'Prescriptions',
    function($scope, $rootScope, $window, $location, $cookieStore, $routeParams, $http, Prescriptions) {
      // var jQuery = $window.jQuery;
      // var prescriptionId = '5920aff3-3936-4130-be9d-aa8b7be39344';
      // var Math = $window.Math;
      // console.log(Math.uuid().toLowerCase());
      var appointmentId = $routeParams.appointmentId;
      var medicationId = $routeParams.medicationId;
      $scope.appointmentId = appointmentId;
      $scope.activePrescriptions = 'rx-active';
      $rootScope.toBeShown = true;
      $rootScope.subNavigationShow = true;

      var patientToken = $cookieStore.get('patientToken');
      // var patientId = $cookieStore.get('patientId');

      Prescriptions.get({
        id: medicationId,
        token: patientToken
      }, function(medication) {
        $scope.medication = medication;
      });

      $scope.updateMedication = function() {
        // console.log($scope.medication);
        Prescriptions.update({
          Id: medicationId,
          token: patientToken,
          prescription: $scope.medication
        }, function() {
          // console.log(medication);
          $location.path('/prescriptions/view/'+appointmentId);
        });
      };

    }
  ]);
