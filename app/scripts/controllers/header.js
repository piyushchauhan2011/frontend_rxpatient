'use strict';

angular.module('frontendRxpatientApp')
  .controller('HeaderCtrl', ['$scope', '$window',
    function($scope, $window) {
      var jQuery = $window.jQuery;
      $scope.calendarOpen = function() {
        // console.log('I\'m opening a calendar for you');
        jQuery('#rx_nav_input_appointment').datepicker();
        jQuery('#rx_nav_input_appointment').slideToggle('fast');
        jQuery('.rx-nav-date').toggleClass('rx-active');
      };

      var weekdays = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
      var months = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
      var date = new Date();
      $scope.currentDate = weekdays[date.getDay()] + ', ' + date.getDate() + ' ' + months[date.getMonth()];

      $scope.dialogOpen = function() {
        // console.log('I just get clicked and opening a dialog for you!.');
        jQuery(window).resize(function() {
          jQuery('#rx_add_appointment').dialog('option', 'position', 'top');
        });
        jQuery('#rx_add_appointment').dialog({
          width: 550,
          height: 420,
          modal: true,
          position: {
            my: 'center',
            at: 'top',
            of: window
          },
          show: {
            effect: 'slideDown',
            duration: 200
          },
          hide: {
            effect: 'slideUp',
            duration: 200
          },
          draggable: false,
          close: function() {
            // $('body').removeClass('rx-oh');
          }
        });
      };
    }
  ]);
