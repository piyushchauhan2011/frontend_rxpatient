'use strict';

angular.module('frontendRxpatientApp')
  .controller('ObstetricsEditCtrl', ['$scope', '$rootScope', '$window', '$location', '$cookieStore', '$routeParams', '$http', 'Obstetrics',
    function($scope, $rootScope, $window, $location, $cookieStore, $routeParams, $http, Obstetrics) {
      // var jQuery = $window.jQuery;
      // var ObstetricsId = '9a61be15-8e60-482d-9f1c-1accacdb00c9';
      var obstetricsId = $routeParams.id;
      $scope.obstetricsId = obstetricsId;
      $scope.activeObstetrics = 'rx-active';
      $rootScope.toBeShown = true;
      $rootScope.subNavigationShow = true;

      var patientToken = $cookieStore.get('patientToken');
      // var patientId = $cookieStore.get('patientId');

      Obstetrics.get({
        id: obstetricsId,
        token: patientToken
      }, function(obstetric) {
        $scope.obstetric = obstetric;
      });

      $scope.updateObstetric = function() {
        delete $scope.obstetric.created_at;
        delete $scope.obstetric.updated_at;

        Obstetrics.update({
          Id: obstetricsId,
          token: patientToken,
          obstetrics_history: $scope.obstetric
        }, function() {
          // console.log(message);
          $location.path('/obstetrics');
        });
      };
    }
  ]);
