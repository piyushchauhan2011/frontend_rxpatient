'use strict';

angular.module('frontendRxpatientApp')
  .controller('VitalsCtrl', ['$scope', '$rootScope', '$cookieStore', '$window', 'Vitals', function($scope, $rootScope, $cookieStore, $window, Vitals) {

    $rootScope.toBeShown = true;
    $rootScope.subNavigationShow = true;
    
    $scope.activeVitals = 'rx-active';
    var patientToken = $cookieStore.get('patientToken');
    // var patientId = $cookieStore.get('patientId');

    $scope.vitals = [];
    Vitals.query({
      token: patientToken
    }, function(vitals) {
      // var vitals = [];
      // var length = $scope.vitals.vitals.length;
      // for(var i=0;i<length;i++) {
      //   vitals.push($scope.vitals.vitals[i].vitals);
      // }
      // $scope.vitals = vitals;
      // console.log("VITALS: " + JSON.stringify(vitals));
      $scope.vitals = vitals;
    });

    var addUpdateVitalBox = angular.element('#rx_add_update_vitals');
    $scope.addUpdateVitals = function() {
      angular.element(window).resize(function() {
        addUpdateVitalBox.dialog('option', 'position', 'top');
      });
      addUpdateVitalBox.dialog({
        width: 600,
        height: 600,
        modal: true,
        position: {
          my: 'center',
          at: 'top',
          of: window
        },
        show: {
          effect: 'slideDown',
          duration: 200
        },
        hide: {
          effect: 'slideUp',
          duration: 200
        },
        draggable: false,
        close: function() {
          // $('body').removeClass('rx-oh');
        }
      });
    };
  }]);
