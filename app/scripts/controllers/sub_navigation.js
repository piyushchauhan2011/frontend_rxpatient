'use strict';

angular.module('frontendRxpatientApp')
  .controller('SubNavigationCtrl', ['$scope', '$rootScope', '$window', '$cookieStore', '$http', 'Map',
    function($scope, $rootScope, $window, $cookieStore, $http, Map) {
      $scope.hitFirstTime = true;
      
      // This is done to get the persisted data on cookie store and only run this one time not again on routeChangeSuccess
      $rootScope.$on('$routeChangeSuccess', function() {
        if($rootScope.topBarShow){
          Map.mapLoad();
        }
        // console.log($rootScope.isUserLoggedIn);
        // console.log($scope.hitFirstTime);
        if($rootScope.isUserLoggedIn && $scope.hitFirstTime) {
          $scope.hitFirstTime = false;
          var patientToken = $cookieStore.get('patientToken');
          var patientId = $cookieStore.get('patientId');
          var patient = $cookieStore.get('patient');
          $scope.patient = patient;
          $scope.avatarUrl = patient.avatar_url;

          // jQuery Nice UI Stuff - Bad Code
          $scope.dropdownCurrentSelection = 'Hospitals';
          $scope.offset = 0;
          var searchResultsDiv = angular.element('.search-results');
          searchResultsDiv.on('mouseover', function() {
            $scope.subMenuStatus = true;
          });
          searchResultsDiv.on('mouseout', function() {
            $scope.subMenuStatus = false;
          });
          searchResultsDiv.hide();
          $scope.subMenuStatus = false;

          var articlesSearchInput = angular.element('#rx_pp_articles_search').find('input');
          $scope.forceSearching = function() { searchResultsDiv.hide(); };
          articlesSearchInput.on('focus', function() {
            if ($scope.subMenuStatus === false) {
              searchResultsDiv.show();
            }
          });
          angular.element(document).click(function(e) {
            if (e.target.localName !== 'input') {
              // console.log('Menu Status: ' + $scope.subMenuStatus);
              if ($scope.subMenuStatus === false) {
                searchResultsDiv.hide();
              }
            }
          });
          // End

          $scope.getSearchResults = function() {
            $http.post($rootScope.articlesApiUrl+'/contents/search?token=' + patientToken, {
              query: $scope.searchArticles,
              offset: $scope.offset
            })
              .success(function(data) {
                $scope.searchResults = data;
              }).error(function() {
                // console.log('Error Sending Request. Please Retry!' + err);
              });
          };
          $scope.searchResultsContents = [];
          $scope.getContent = function(articleId) {
            $http.get($rootScope.articlesApiUrl+'/articles/get_article/' + articleId + '?token=' + patientToken)
              .success(function(content) {
                $scope.searchResultsContents[articleId] = content;
              }).error(function() {
                // console.log(err);
              });
          };

          // Autocomplete for Hospitals/Locations
          var locationInput = document.getElementById('location_input');
          var options = {
            // types: ['(cities)'],
            componentRestrictions: {
              country: 'in'
            }
          };

          var bar = angular.element('.bar');
          var percent = angular.element('.percent');
          var status = angular.element('#status');

          $rootScope.imageUrl = $rootScope.patientApiUrl+'/patients/'+patientId+'?token='+patientToken;

          angular.element('#edit_image_uploader form').ajaxForm({
            url: $rootScope.patientApiUrl+'/patients/'+patientId+'?token='+patientToken,
            type: 'put',
            beforeSubmit: function() {
                // console.log(formData);
                // console.log($form);
                status.empty();
                var percentVal = '0%';
                bar.width(percentVal);
                percent.html(percentVal);
            },
            uploadProgress: function(event, position, total, percentComplete) {
                var percentVal = percentComplete + '%';
                bar.width(percentVal);
                percent.html(percentVal);
            //console.log(percentVal, position, total);
            },
            success: function() {
                var percentVal = '100%';
                bar.width(percentVal);
                percent.html(percentVal);
            },
            complete: function() {
              // status.html(xhr.responseText);
              status.html('Successfully saved your pic.');
            }
          });
          
          // Search Bar Code
          // / Jquery UI Code
          var hospitalsSearchBar = angular.element('#rx_pp_hospitals_search');
          var articlesSearchBar = angular.element('#rx_pp_articles_search');
          
          var hospitalsItem = angular.element('#hs_item_hospitals');
          var articlesItem = angular.element('#hs_item_articles');
          // hide all search bars
          var hideSearchBars = function() {
            hospitalsSearchBar.hide();
            articlesSearchBar.hide();

            hospitalsItem.removeClass('active');
            articlesItem.removeClass('active');
          };
          // Functions
          $scope.showHospitalsSearch = function() {
            if(!hospitalsItem.hasClass('active')) {
              hideSearchBars();
              hospitalsItem.addClass('active');
              hospitalsSearchBar.show('bounce');
              $scope.dropdownCurrentSelection = 'Hospitals';
            }
          };
          $scope.showHospitalsSearch();
          $scope.showArticlesSearch = function() {
            if(!articlesItem.hasClass('active')) {
              hideSearchBars();
              articlesItem.addClass('active');
              articlesSearchBar.show('bounce');
              $scope.dropdownCurrentSelection = 'Articles';
            }
          };

          // / Google Places Stuff for search bar
          try {
          var google = $window.google;
          var searchBox = new google.maps.places.Autocomplete(locationInput, options);
          google.maps.event.addListener(searchBox, 'places_changed', function() {
            // var places = searchBox.getPlace();
            // console.log(places);
          });
          } catch(e) {}
          // End Search Bar code


          // Profile Pic Code
          var profilePicEl = angular.element('#rx_pp_pi_img');
          var editImageButton = profilePicEl.find('.edit-image-button');
          editImageButton.hide();
          profilePicEl.hover(function() {
            editImageButton.fadeIn('fast');
          }, function() {
            editImageButton.fadeOut('fast');
          });
          var editImageUploader = angular.element('#edit_image_uploader');
          // Upload Image Avatar JS
          $scope.openUploader = function() {
            // console.log('I just get clicked and opening a uploader dialog for you!.');
            angular.element(window).resize(function() {
              editImageUploader.dialog('option', 'position', 'top');
            });
            editImageUploader.dialog({
              width: 450,
              height: 180,
              modal: true,
              position: {
                my: 'center',
                at: 'top',
                of: window
              },
              show: {
                effect: 'slideDown',
                duration: 200
              },
              hide: {
                effect: 'slideUp',
                duration: 200
              },
              draggable: false,
              close: function() {
                // $('body').removeClass('rx-oh');
              }
            });
          };
        }
      });
    }
  ]);
