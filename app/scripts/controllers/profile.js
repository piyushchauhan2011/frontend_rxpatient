'use strict';

angular.module('frontendRxpatientApp')
  .controller('ProfileCtrl', ['$scope', '$rootScope', '$window', '$cookieStore', '$http', 'Patients', 'Vitals', 'Allergies', 'Conditions', 'Prescriptions', 'Families',
    function($scope, $rootScope, $window, $cookieStore, $http, Patients, Vitals, Allergies, Conditions, Prescriptions, Families) {
      // var jQuery = $window.jQuery;

      $rootScope.toBeShown = true;
      $rootScope.subNavigationShow = true;
      $rootScope.subNavigationProfile = 'active';

      var token = $cookieStore.get('patientToken');
      // var id = $cookieStore.get('patientId');
      var patient = $cookieStore.get('patient');
      $scope.patient = patient;

      $scope.vitals = [];
      Vitals.query({
        token: token
      }, function(vitals) {
        $scope.vitals = vitals;
      });

      $scope.allergies = Allergies.query({
        token: token
      }, function() {
      });

      $scope.conditions = Conditions.query({
        token: token
      }, function() {
      });

      $scope.prescriptions = Prescriptions.query({
        token: token
      }, function() {});

      Families.query({
        token: token
      }, function(families) {
        $scope.families = families;
        // console.log(families);
        $scope.relations = [];
        for (var i = 0; i < families.length; i++) {
          var family = families[i];
          getRelation(family, token);
        }
      });

      var getRelation = function(family, token) {
        $http.get($rootScope.patientApiUrl+'/families/member/' + family.member_id + '?token=' + token)
          .success(function(relation) {
            // console.log(relation);
            $scope.relations.push(relation);
          })
          .error(function(error) {
            $scope.relations.push(error);
          });
      };
    }
  ]);
