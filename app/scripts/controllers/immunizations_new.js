'use strict';

angular.module('frontendRxpatientApp')
  .controller('ImmunizationsNewCtrl', ['$scope', '$rootScope', '$window', '$location', '$cookieStore', '$routeParams', '$http', 'Immunizations',
    function($scope, $rootScope, $window, $location, $cookieStore, $routeParams, $http, Immunizations) {
      // var jQuery = $window.jQuery;
      // var allergyId = '9a61be15-8e60-482d-9f1c-1accacdb00c9';
      $scope.activeImmunizations = 'rx-active';
      $rootScope.toBeShown = true;
      $rootScope.subNavigationShow = true;

      var patientToken = $cookieStore.get('patientToken');
      // var patientId = $cookieStore.get('patientId');

      $scope.createImmunization = function() {
        // console.log($scope.immunization);
        Immunizations.save({
          token: patientToken,
          immunization_history: $scope.immunization
        }, function() {
          // console.log(message);
          $location.path('/immunizations');
        });
      };
    }
  ]);
