'use strict';

angular.module('frontendRxpatientApp')
  .controller('AllergiesCtrl', ['$scope', '$rootScope', '$window', '$location', '$cookieStore', '$routeParams', '$http', 'Allergies',
    function($scope, $rootScope, $window, $location, $cookieStore, $routeParams, $http, Allergies) {
      // var jQuery = $window.jQuery;
      var allergyId = '9a61be15-8e60-482d-9f1c-1accacdb00c9';
      $scope.allergyId = allergyId;
      $scope.activeAllergies = 'rx-active';
      $rootScope.toBeShown = true;
      $rootScope.subNavigationShow = true;

      var patientToken = $cookieStore.get('patientToken');
      // var patientId = $cookieStore.get('patientId');

      Allergies.query({
        token: patientToken
      }, function(allergies) {
        $scope.allergies = allergies;
      });

      $scope.navigateToAllergy = function(id) {
        $location.path('/allergies/edit/'+id);
      };

      $scope.addNewAllergy = function() {
        $location.path('/allergies/new');
      };
    }
  ]);
