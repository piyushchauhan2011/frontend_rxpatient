'use strict';

angular.module('frontendRxpatientApp')
  .controller('InsurancesIndexCtrl', ['$scope', '$rootScope', '$window', '$location', '$cookieStore', '$routeParams', '$http', 'Insurances',
    function($scope, $rootScope, $window, $location, $cookieStore, $routeParams, $http, Insurances) {
      // var jQuery = $window.jQuery;
      $scope.activeInsurances = 'rx-active';
      $rootScope.toBeShown = true;
      $rootScope.subNavigationShow = true;

      var patientToken = $cookieStore.get('patientToken');
      // var patientId = $cookieStore.get('patientId');

      Insurances.query({
        token: patientToken
      }, function(insurances) {
        // var temp = conditions;
        $scope.insurances = insurances;
        // temp[0].parameters = { 'feeling': 'hello world', 'psych-level': 50};
        // console.log(temp);
      });
      $scope.predicate = 'status';

      $scope.navigateToInsurance = function(id) {
        $location.path('/insurances/edit/'+id);
      };
      $scope.addNewInsurance = function() {
        $location.path('/insurances/new');
      };
    }
  ]);
