'use strict';

angular.module('frontendRxpatientApp')
  .controller('InsurancesEditCtrl', ['$scope', '$rootScope', '$window', '$cookieStore', '$routeParams', '$http', 'Insurances',
    function($scope, $rootScope, $window, $cookieStore, $routeParams, $http, Insurances) {
      var jQuery = $window.jQuery;
      var insuranceId = $routeParams.id;
      $scope.activeInsurances = 'rx-active';
      $rootScope.toBeShown = true;
      $rootScope.subNavigationShow = true;

      var patientToken = $cookieStore.get('patientToken');

      $scope.ptempKey = [];
      $scope.ptempValue = [];
      Insurances.get({
        id: insuranceId,
        token: patientToken
      }, function(insurance) {
        $scope.insurance = insurance;
        jQuery.each($scope.insurance.coverages, function(key, value) {
          $scope.ptempKey.push(key);
          $scope.ptempValue.push(value);
        });
      });
      $scope.predicate = 'status';
      $scope.coverageOptions = ['a', 'b', 'c', 'd'];

      $scope.deleteCoverage = function(position) {
        $scope.ptempKey.splice(position,1);
        $scope.ptempValue.splice(position,1);
      };

      $scope.insuranceUpdate = function() {
        $scope.insurance.coverages = {};
        for(var i=0;i<$scope.ptempKey.length;i++) {
          $scope.insurance.coverages[$scope.ptempKey[i]] = $scope.ptempValue[i];
        }
        delete $scope.insurance.id;
        delete $scope.insurance.created_at;
        delete $scope.insurance.updated_at;

        Insurances.update({
          Id: insuranceId,
          token: patientToken,
          insurance: $scope.insurance
        }, function() {
        });
      };
    }
  ]);
