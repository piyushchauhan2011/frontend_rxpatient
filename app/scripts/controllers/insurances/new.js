'use strict';

angular.module('frontendRxpatientApp')
  .controller('InsurancesNewCtrl', ['$scope', '$rootScope', '$window', '$location', '$cookieStore', '$routeParams', '$http', 'Insurances',
    function($scope, $rootScope, $window, $location, $cookieStore, $routeParams, $http, Insurances) {

      $scope.activeInsurances = 'rx-active';
      $rootScope.toBeShown = true;
      $rootScope.subNavigationShow = true;

      var patientToken = $cookieStore.get('patientToken');

      $scope.ptempKey = [];
      $scope.ptempValue = [];

      $scope.predicate = 'status';
      $scope.parameterOptions = ['a', 'b', 'c', 'd'];

      $scope.deleteCoverage = function(position) {
        $scope.ptempKey.splice(position,1);
        $scope.ptempValue.splice(position,1);
      };

      $scope.createInsurance = function() {
        $scope.insurance.coverages = {};
        for(var i=0;i<$scope.ptempKey.length;i++) {
          $scope.insurance.coverages[$scope.ptempKey[i]] = $scope.ptempValue[i];
        }

        Insurances.save({
          token: patientToken,
          insurance: $scope.insurance
        }, function() {
          $location.path('/insurances');
        });
      };
    }
  ]);
