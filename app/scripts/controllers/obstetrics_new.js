'use strict';

angular.module('frontendRxpatientApp')
  .controller('ObstetricsNewCtrl', ['$scope', '$rootScope', '$window', '$location', '$cookieStore', '$routeParams', '$http', 'Obstetrics',
    function($scope, $rootScope, $window, $location, $cookieStore, $routeParams, $http, Obstetrics) {
      // var jQuery = $window.jQuery;
      // var allergyId = '9a61be15-8e60-482d-9f1c-1accacdb00c9';
      $scope.activeObstetrics = 'rx-active';
      $rootScope.toBeShown = true;
      $rootScope.subNavigationShow = true;

      var patientToken = $cookieStore.get('patientToken');
      // var patientId = $cookieStore.get('patientId');

      $scope.createObstetric = function() {
        // console.log($scope.obstetric);
        Obstetrics.save({
          token: patientToken,
          obstetrics_history: $scope.obstetric
        }, function() {
          // console.log(message);
          $location.path('/obstetrics');
        });
      };
    }
  ]);
