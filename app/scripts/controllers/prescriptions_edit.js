'use strict';

angular.module('frontendRxpatientApp')
  .controller('PrescriptionsEditCtrl', ['$scope', '$rootScope', '$window', '$cookieStore', '$routeParams', '$http', 'Prescriptions',
    function($scope, $rootScope, $window, $cookieStore, $routeParams, $http, Prescriptions) {
      // var jQuery = $window.jQuery;
      // var prescriptionId = '5920aff3-3936-4130-be9d-aa8b7be39344';
      var prescriptionId = $routeParams.id;
      $scope.prescriptionId = prescriptionId;
      $scope.activePrescriptions = 'rx-active';
      $rootScope.toBeShown = true;
      $rootScope.subNavigationShow = true;

      var patientToken = $cookieStore.get('patientToken');
      // var patientId = $cookieStore.get('patientId');

      Prescriptions.get({
        id: prescriptionId,
        token: patientToken
      }, function(prescription) {
        $scope.prescription = prescription;
      });

    }
  ]);
