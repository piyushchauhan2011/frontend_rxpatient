'use strict';

angular.module('frontendRxpatientApp')
  .controller('NearbyCtrl', ['$scope', '$window', '$http', '$rootScope',
    function($scope, $window, $http, $rootScope) {
      var jQuery = $window.jQuery;
      var google = $window.google;
      $rootScope.mapLoaded = true;
      // var InfoBubble = $window.InfoBubble;
      $scope.awesomeThings = [
        'HTML5 Boilerplate',
        'AngularJS',
        'Karma'
      ];
      $scope.resultsActive = true;
      $scope.shortlistsActive = false;

      var mapCanvas = jQuery('#map_canvas');

      mapCanvas.css('z-index', 0);
      mapCanvas.css('height', '100%');
      jQuery('#footer').css('display', 'none');

      var locationNearbyInput = document.getElementById('location_nearby_input');
      var options = {
        // types: ['(cities)'],
        componentRestrictions: {
          country: 'in'
        }
      };
      var searchBox = new google.maps.places.Autocomplete(locationNearbyInput, options);
      // For resizing the map canvas to fit with in the map. Initially map was displaying
      // in half of the area and somewhat not occupying the whole area.
      var map_canvas = document.getElementById('map_canvas');

      google.maps.event.trigger(map_canvas, 'resize');

      var initMaps = function(position) {
        // console.log(position.coords.latitude);
        // console.log(position.coords.longitude);
        var url = $rootScope.locationApiUrl + '/hospitals_latlng/?jsoncallback=true';
        // var tCenter = {
        //   k: 26.8373174,
        //   A: 75.8075002
        // };
        var pCenter = {
          k: position.coords.latitude,
          A: position.coords.longitude
        };
        $http.get(url, {
          params: {
            radius: 2000,
            center: pCenter
          }
        })
          .success(function(data) {
            var Latlng = new google.maps.LatLng(pCenter.k, pCenter.A);
            var mapOptions = {
              center: Latlng,
              zoom: 14
            };
            var map = new google.maps.Map(map_canvas, mapOptions);
            var styles = [{
              featureType: 'poi',
              stylers: [{
                visibility: 'off'
              }]
            }];
            map.setOptions({styles: styles});
            map.panBy(-50, -50);

            var image = 'https://s3-ap-southeast-1.amazonaws.com/rxhealth-images/hospital-2.png';
            var marker = new google.maps.Marker({
              position: Latlng,
              map: map,
              icon: image
            });
            marker.setMap(map);

            var contentString = 'Some String';
            var infowindow = new google.maps.InfoWindow({
              content: contentString,
              disableAutoPan: false
            });
            google.maps.event.addListener(marker, 'click', function() {
              infowindow.open(marker.get('map'), marker);
            });
            // google.maps.event.addListener(marker, 'mouseover', function() {
            //   infowindow.open(marker.get('map'), marker);
            // });
            // google.maps.event.addListener(marker, 'mouseout', function() {
            //   infowindow.close(marker.get('map'), marker);
            // });

            var populationOptions = {
              strokeColor: '#074f68',
              strokeOpacity: 0.8,
              strokeWeight: 1,
              fillColor: '#074e68',
              fillOpacity: 0.35,
              map: map,
              center: Latlng,
              radius: 2000,
              editable: true,
              icon: image
            };
            //var tmp = [];

            //console.log(data);
            var d;
            var markers = [];
            var latlngs = [];
            var hospitals = JSON.parse(data[1]);
            $scope.hospitals = hospitals;
            // console.log(hospitals);
            // console.log(hospitals[0].fields.address);
            for (var i = 0; i < data[0].length; i++) {
              d = JSON.parse(data[0][i]);
              latlngs[i] = new google.maps.LatLng(d.coordinates[1], d.coordinates[0]);
              var tmp = new google.maps.Marker({
                position: latlngs[i],
                map: map,
                icon: image,
                title: 'Hello World! - ' + i
              });
              tmp.setMap(map);
              markers[i] = tmp;
            }
            var hospitalCircle = new google.maps.Circle(populationOptions);
            google.maps.event.addListener(hospitalCircle, 'radius_changed', function() {
              // console.log(hospitalCircle.getRadius());

              // Clear all markers
              for (var i = 0; i < markers.length; i++) {
                markers[i].setMap(null);
              }
              markers = [];
              latlngs = [];

              // Get new markers data
              $http.get(url, {
                params: {
                  radius: hospitalCircle.getRadius(),
                  center: hospitalCircle.getCenter()
                }
              })
                .success(function(data) {
                  // console.log(data);
                  $scope.hospitals = JSON.parse(data[1]);
                  for (var i = 0; i < data[0].length; i++) {
                    d = JSON.parse(data[0][i]);
                    // console.log(d);
                    latlngs[i] = new google.maps.LatLng(d.coordinates[1], d.coordinates[0]);
                    var tmp = new google.maps.Marker({
                      position: latlngs[i],
                      map: map,
                      icon: image
                    });
                    tmp.setMap(map);
                    markers[i] = tmp;
                    // console.log(markers);
                    // console.log(latlngs);
                  }
                });
            });
            google.maps.event.addListener(hospitalCircle, 'center_changed', function() {
              // console.log(hospitalCircle.getRadius());
              // console.log(hospitalCircle.getCenter());

              // Clear all markers
              for (var i = 0; i < markers.length; i++) {
                markers[i].setMap(null);
              }
              markers = [];
              latlngs = [];

              // Get new markers data
              $http.get(url, {
                params: {
                  radius: hospitalCircle.getRadius(),
                  center: hospitalCircle.getCenter()
                }
              })
                .success(function(data) {
                  // console.log(data);
                  $scope.hospitals = JSON.parse(data[1]);
                  for (var i = 0; i < data[0].length; i++) {
                    d = JSON.parse(data[0][i]);
                    // console.log(d);
                    latlngs[i] = new google.maps.LatLng(d.coordinates[1], d.coordinates[0]);
                    var tmp = new google.maps.Marker({
                      position: latlngs[i],
                      map: map,
                      icon: image
                    });
                    tmp.setMap(map);
                    markers[i] = tmp;
                  }
                  // console.log(markers);
                  // console.log(latlngs);
                });
            });
            
            google.maps.event.addListener(searchBox, 'place_changed', function() {
              var place = searchBox.getPlace();
              // console.log(place.geometry.location.A);
              // console.log(place.geometry.location.k);

              map.setCenter(place.geometry.location);
              map.panBy(-50, -50);
              hospitalCircle.setCenter(place.geometry.location);
            });
          });
      };

      if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(function(position) {
          initMaps(position);
        });
      } else {
        var position = {};
        position.coords = {};
        position.coords.latitude = 26.8373174;
        position.coords.longitude = 75.8075002;
        initMaps(position);
      }
    }
  ]);
