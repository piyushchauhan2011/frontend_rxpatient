'use strict';

angular.module('frontendRxpatientApp')
  .controller('FamiliesCtrl', ['$scope', '$window', '$cookieStore', '$routeParams', '$http', '$rootScope', 'Families',
    function($scope, $window, $cookieStore, $routeParams, $http, $rootScope, Families) {
      // var jQuery = $window.jQuery;
      $scope.activeFamilies = 'rx-active';

      var patientToken = $cookieStore.get('patientToken');
      // var patientId = $cookieStore.get('patientId');

      Families.query({
        token: patientToken
      }, function(families) {
        $scope.families = families;


        var relation = families[0];
        $http.get($rootScope.patientApiUrl+'/families/member/' + relation.member_id + '?token=' + patientToken)
          .success(function(relation) {
            $scope.relation = relation;
            // console.log(relation);
          })
          .error(function(error) {
            $scope.relation = error;
          });
      });

      // var newToken;
      // $http.post('http://localhost:3000/patients/login', {
      //   email: 'piyushchauhan2011@gmail.com',
      //   password: 'diehard4'
      // })
      // .success(function(data) {
      //   newToken = data;
      //   var idata = {
      //     insurance_plan: "HelloWorld",
      //     policy_no: "dk234",
      //     valid_upto: new Date(),
      //     insurance_provider: "MAXLIFE",
      //     coverages: {
      //       hospital: "APEX",
      //       janitor: "RMJ"
      //     },
      //     name_tpa: "Hello Not"
      //   };
      //   $http.post('http://localhost:3000/insurances?token=' + newToken, idata);
      // })
      // .error(function(error) {})
    }
  ]);
