'use strict';

angular.module('frontendRxpatientApp')
  .controller('ObstetricsCtrl', ['$scope', '$rootScope', '$window', '$location', '$cookieStore', '$routeParams', '$http', 'Obstetrics',
    function($scope, $rootScope, $window, $location, $cookieStore, $routeParams, $http, Obstetrics) {

      $scope.activeObstetrics = 'rx-active';
      $rootScope.toBeShown = true;
      $rootScope.subNavigationShow = true;

      var patientToken = $cookieStore.get('patientToken');
      // var patientId = $cookieStore.get('patientId');

      Obstetrics.query({
        token: patientToken
      }, function(obstetrics) {
        $scope.obstetrics = obstetrics;
      });

      $scope.navigateToObstetric = function(id) {
        $location.path('/obstetrics/edit/'+id);
      };

      $scope.addNewObstetric = function() {
        $location.path('/obstetrics/new');
      };
    }
  ]);
