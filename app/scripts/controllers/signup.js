'use strict';

angular.module('frontendRxpatientApp')
  .controller('SignupCtrl', ['$scope', '$rootScope', '$window', '$location', 'Patients', function ($scope, $rootScope, $window, $location, Patients) {
    var head = $window.head;
    //Wrapping the Recaptcha create method in a javascript function
    head.load('//www.google.com/recaptcha/api/js/recaptcha_ajax.js', function() {
      var Recaptcha = $window.Recaptcha;
      function showRecaptcha(element) {
        Recaptcha.create('6LcPofMSAAAAAGmJg72nN52RA-E0xuObIbAh2klz', element, {
          theme: 'white',
          callback: Recaptcha.focus_response_field
        });
      }
      showRecaptcha('recaptcha_div');
    });
    var jQuery = $window.jQuery;
    // var Recaptcha = $window.Recaptcha;
    $rootScope.toBeShown = false;

    $scope.signup = function(patient, valid) {
      // console.log('Signing up the patient.');
      // console.log(Recaptcha.get_response());
      // console.log(Recaptcha.get_challenge());
      if(valid) {
        var data = {};
        data.patient = angular.copy(patient);
        // console.log('PAYLOAD SENT: ' + JSON.stringify(data));
        Patients.save(data, function() {
          // console.log('RESPONSE DATA: ' + JSON.stringify(data));
          $rootScope.bErrors = true;
          $rootScope.cErrors = 'Successfully Signed Up. Check your email for confirmation.';
          $location.path('/');
        }, function(err) {
          // console.log('RESPONSE ERR: ' + JSON.stringify(err));
          if(err.data.email) { $scope.alreadyRegistered = true; }
        });
      } else {
        // console.log('Correct Errors');
      }
    };

    $scope.bloodGroups = ['O-', 'O+', 'A-', 'A+', 'B-', 'B+', 'AB-', 'AB+'];
    $scope.phoneTypes = ['Home', 'Mobile', 'Work'];
    var rxSRadio = jQuery('.rx-sradio');
    $scope.addFocusClass = function() {
      if(!rxSRadio.hasClass('focus')) {
        rxSRadio.addClass('focus');
      }
    };
    $scope.removeFocusClass = function() {
      rxSRadio.removeClass('focus');
    };
  }]);
