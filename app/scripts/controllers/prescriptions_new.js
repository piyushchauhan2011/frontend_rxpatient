'use strict';

angular.module('frontendRxpatientApp')
  .controller('PrescriptionsNewCtrl', ['$scope', '$rootScope', '$window', '$location',
    function($scope, $rootScope, $window, $location) {
      // var jQuery = $window.jQuery;
      var Math = $window.Math;
      // var prescriptionId = '5920aff3-3936-4130-be9d-aa8b7be39344';
      var prescriptionId = Math.uuid().toLowerCase();
      $scope.prescriptionId = prescriptionId;
      $scope.activePrescriptions = 'rx-active';
      $rootScope.toBeShown = true;
      $rootScope.subNavigationShow = true;

      // var patientToken = $cookieStore.get('patientToken');
      // var patientId = $cookieStore.get('patientId');

      $scope.navigateToMedication = function(id) {
        $location.path('/prescriptions/view/'+ prescriptionId + '/edit/' + id);
      };

      $scope.addNewMedication = function() {
        $location.path('/prescriptions/view/'+prescriptionId+'/new');
      };

    }
  ]);
