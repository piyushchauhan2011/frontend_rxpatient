'use strict';

angular.module('frontendRxpatientApp')
  .controller('PrescriptionsCtrl', ['$scope', '$rootScope', '$window', '$location', '$cookieStore', '$routeParams', '$http',
    function($scope, $rootScope, $window, $location, $cookieStore, $routeParams, $http) {
      // var jQuery = $window.jQuery;
      // var prescriptionId = '5920aff3-3936-4130-be9d-aa8b7be39344';
      // $scope.prescriptionId = prescriptionId;
      $scope.activePrescriptions = 'rx-active';
      $rootScope.toBeShown = true;
      $rootScope.subNavigationShow = true;

      var patientToken = $cookieStore.get('patientToken');
      // var patientId = $cookieStore.get('patientId');

      // Prescriptions.query({
      //   token: patientToken
      // }, function(prescriptions) {
      //   $scope.prescriptions = prescriptions;
      // });

      $scope.prescriptions = {};

      $http.get($rootScope.patientApiUrl+
        '/prescriptions/appointment_ids?token=' + patientToken
      ).success(function(ids) {
        $scope.appointmentIds = ids;
        // console.log(ids);
        for(var i=0;i<ids.length;i++) {
          getPrescription(ids[i]);
        }
      }).error(function() {
        // console.log(err);
      });

      var getPrescription = function(prescriptionId) {
        $http.get($rootScope.patientApiUrl+
          '/prescriptions/prescription/' + prescriptionId + '?token=' + patientToken
        ).success(function(medications) {
          $scope.prescriptions[prescriptionId] = medications;
          // console.log($scope.prescriptions[prescriptionId]);
        }).error(function() {
          // console.log(err);
        });
      };

      $scope.showPrescription = function(id) {
        $location.path('/prescriptions/view/'+id);
      };
      $scope.addNewPrescription = function() {
        $location.path('/prescriptions/new');
      };
    }
  ]);
