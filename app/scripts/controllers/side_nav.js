'use strict';

angular.module('frontendRxpatientApp')
  .controller('SideNavCtrl', ['$scope', '$rootScope', '$cookieStore',
    function($scope, $rootScope, $cookieStore) {
      // var patientToken = $cookieStore.get('patientToken');
      // var patientId = $cookieStore.get('patientId');
      $scope.patient = $cookieStore.get('patient');
    }
  ]);