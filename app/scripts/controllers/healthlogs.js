'use strict';

angular.module('frontendRxpatientApp')
  .controller('HealthlogsCtrl', ['$scope', '$rootScope', function ($scope, $rootScope) {
    $scope.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];

    // Layout Variables
    $rootScope.toBeShown = true;
    $rootScope.subNavigationShow = true;
    $rootScope.subNavigationHealthlogs = 'active';

    // Other work
  }]);
