'use strict';

angular.module('frontendRxpatientApp')
  .controller('PrescriptionsViewCtrl', ['$scope', '$rootScope', '$window', '$location', '$cookieStore', '$routeParams', '$http',
    function($scope, $rootScope, $window, $location, $cookieStore, $routeParams, $http) {
      // var jQuery = $window.jQuery;
      // var prescriptionId = '5920aff3-3936-4130-be9d-aa8b7be39344';
      var prescriptionId = $routeParams.appointmentId;
      $scope.prescriptionId = prescriptionId;
      $scope.activePrescriptions = 'rx-active';
      $rootScope.toBeShown = true;
      $rootScope.subNavigationShow = true;

      var patientToken = $cookieStore.get('patientToken');
      // var patientId = $cookieStore.get('patientId');

      $http.get($rootScope.patientApiUrl+
        '/prescriptions/prescription/'+prescriptionId+'?token='+patientToken
      ).success(function(medications) {
        $scope.medications = medications;
        // console.log(medications);
      }).error(function(err) { console.log(err); });

      $scope.navigateToMedication = function(id) {
        $location.path('/prescriptions/view/'+ prescriptionId + '/edit/' + id);
      };

      $scope.addNewMedication = function() {
        $location.path('/prescriptions/view/'+prescriptionId+'/new');
      };

    }
  ]);
