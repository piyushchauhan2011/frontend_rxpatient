'use strict';

angular.module('frontendRxpatientApp')
  .controller('FeedbackCtrl', ['$scope', '$window', '$http', '$rootScope', 'Map',
    function($scope, $window, $http, $rootScope, Map) {
      Map.mapLoad();
      $rootScope.topBarShow = false;
      var jQuery = $window.jQuery;
      jQuery('.map-hide').css('display', 'block');
      var mapCanvas = jQuery('#map_canvas');
      mapCanvas.css('height', '100%');
      mapCanvas.css('z-index', 0);
      angular.element('#footer').hide();

      $scope.feedback = {};
      $scope.feedback.source = [];
      $scope.feedback.newFeatures = [];
      $scope.setRecommendation = function(value) {
        $scope.feedback.recommendation = value;
      };
      $scope.setUsability = function(value) {
        $scope.feedback.usability = value;
      };
      $scope.sendFeedback = function() {
        // console.log($scope.feedback);
        $http.post($rootScope.feedbackApiUrl+'/feedback/create', $scope.feedback)
          .success(function() {
            // console.log('Saved: ' + data);
            $rootScope.bErrors = true;
            $rootScope.cErrors = 'Successfully saved your Feedback!';
            $scope.feedback = {};
            $scope.feedback.source = [];
            $scope.feedback.newFeatures = [];
          })
          .error(function() {
            // console.log(err);
          });
      };
      $scope.closeFeedback = function() {
        // angular.element('html,body').animate({scrollTop: 0}, 'fast');
        $window.history.back();
        // $location.path('index');
      };
    }
  ]);
