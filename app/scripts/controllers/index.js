'use strict';

angular.module('frontendRxpatientApp')
  .controller('IndexCtrl', ['$scope', '$window', '$rootScope', '$cookieStore', '$location', '$http', '$routeParams', 'Patients', 'Conditions',
    function($scope, $window, $rootScope, $cookieStore, $location, $http, $routeParams, Patients, Conditions) {

      $scope.awesomeThings = ['hello'];

      $rootScope.toBeShown = true;
      $rootScope.subNavigationShow = true;
      $rootScope.subNavigationIndex = 'active';
      $scope.url = $location.url();
      if($scope.url === '/index') { $scope.url += '?'; }

      var patientToken = $cookieStore.get('patientToken');

      // var patientId = $cookieStore.get('patientId');

      $scope.articlesLimit = 10;

      var keywords;
      if($routeParams.tags === undefined) {
        keywords = $scope.patient.article_keywords;
      } else {
        keywords = $routeParams.tags;
      }
      $http.post($rootScope.articlesApiUrl+'/articles/tagged_articles', {tags: keywords})
      .success(function(articles) {
        $scope.articles = articles;
        // console.log(articles);
      });

      $scope.conditions = Conditions.query({
        token: patientToken
      }, function() {});

      $scope.articlesFavLikes = [];
      $scope.fetchFavLikes = function(articleId) {
        $http.get($rootScope.patientApiUrl+'/articles/patient_article/'+articleId+'/?token='+patientToken)
        .success(function(articleFavLikes) {
          $scope.articlesFavLikes[articleId] = articleFavLikes;
        })
        .error(function() { });
      };
      var article;

      $scope.useful = function(articleId) {
        if($scope.articlesFavLikes[articleId].length === 0) {
          $scope.articlesFavLikes[articleId][0] = {};
          article = $scope.articlesFavLikes[articleId][0];
          article.article_ref = articleId;
          article.likes = 1; article.favourite = false;
          $http.post($rootScope.patientApiUrl+'/articles?token='+patientToken, {
            article: article
          })
            .success(function(data) {
              // console.log("Successfully Created");
              // console.log(data);
              article.id = data.id;
            })
            .error(function() {
              // console.log(err);
            });
        } else {
          article = $scope.articlesFavLikes[articleId][0];
          if(article.likes === 1) { article.likes = 0; }
          else { article.likes = 1; }
          $http.put($rootScope.patientApiUrl+'/articles/'+article.id+'?token='+patientToken, {
            article: article
          })
            .success(function() {
              // console.log('Successfully Updated');
              // console.log(data);
            })
            .error(function() {
              // console.log(err);
            });
        }
      };

      $scope.notUseful = function(articleId) {
        if($scope.articlesFavLikes[articleId].length === 0) {
          $scope.articlesFavLikes[articleId][0] = {};
          article = $scope.articlesFavLikes[articleId][0];
          article.article_ref = articleId;
          article.likes = -1; article.favourite = false;
          $http.post($rootScope.patientApiUrl+'/articles?token='+patientToken, {
            article: article
          })
            .success(function(data) {
              // console.log("Successfully Created");
              // console.log(data);
              article.id = data.id;
            })
            .error(function() {
              // console.log(err);
            });
        } else {
          article = $scope.articlesFavLikes[articleId][0];
          if(article.likes === -1) { article.likes = 0; }
          else { article.likes = -1; }
          $http.put($rootScope.patientApiUrl+'/articles/'+article.id+'?token='+patientToken, {
            article: article
          })
            .success(function() {
              // console.log('Successfully Updated');
              // console.log(data);
            })
            .error(function() {
              // console.log(err);
            });
        }
      };
      
      $scope.favourite = function(articleId) {
        console.log($scope.articlesFavLikes[articleId].length);
        if($scope.articlesFavLikes[articleId].length === 0) {
          $scope.articlesFavLikes[articleId][0] = {};
          article = $scope.articlesFavLikes[articleId][0];
          article.article_ref = articleId;
          article.favourite = true; article.likes = 0;
          $http.post($rootScope.patientApiUrl+'/articles?token='+patientToken, {
            article: article
          })
            .success(function(data) {
              // console.log("Successfully Created");
              // console.log(data);
              article.id = data.id;
            })
            .error(function() {
              // console.log(err);
            });
        } else {
          article = $scope.articlesFavLikes[articleId][0];
          if(article.favourite === true) { article.favourite = false; }
          else { article.favourite = true; }
          $http.put($rootScope.patientApiUrl+'/articles/'+article.id+'?token='+patientToken, {
            article: article
          })
            .success(function() {
              // console.log('Successfully Updated');
              // console.log(data);
            })
            .error(function() {
              // console.log(err);
            });
        }
      };
    }
  ]);
