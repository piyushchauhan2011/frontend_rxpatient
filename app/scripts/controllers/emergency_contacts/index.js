'use strict';

angular.module('frontendRxpatientApp')
  .controller('EmergencyContactsIndexCtrl', ['$scope', '$rootScope', '$window', '$location', '$cookieStore', '$routeParams', '$http', 'EmergencyContacts',
    function($scope, $rootScope, $window, $location, $cookieStore, $routeParams, $http, EmergencyContacts) {
      // var jQuery = $window.jQuery;
      
      $scope.activeEmergencyContacts = 'rx-active';
      $rootScope.toBeShown = true;
      $rootScope.subNavigationShow = true;

      var patientToken = $cookieStore.get('patientToken');
      // var patientId = $cookieStore.get('patientId');

      EmergencyContacts.query({
        token: patientToken
      }, function(emergencyContacts) {
        $scope.emergencyContacts = emergencyContacts;
      });

      $scope.navigateToEmergencyContact = function(id) {
        $location.path('/emergency_contacts/edit/'+id);
      };

      $scope.addNewEmergencyContact = function() {
        $location.path('/emergency_contacts/new');
      };
    }
  ]);
