'use strict';

angular.module('frontendRxpatientApp')
  .controller('EmergencyContactsNewCtrl', ['$scope', '$rootScope', '$window', '$location', '$cookieStore', '$routeParams', '$http', 'EmergencyContacts',
    function($scope, $rootScope, $window, $location, $cookieStore, $routeParams, $http, EmergencyContacts) {
      // var jQuery = $window.jQuery;
      $scope.activeEmergencyContacts = 'rx-active';
      $rootScope.toBeShown = true;
      $rootScope.subNavigationShow = true;

      var patientToken = $cookieStore.get('patientToken');
      // var patientId = $cookieStore.get('patientId');

      $scope.createEmergencyContact = function() {
        EmergencyContacts.save({
          token: patientToken,
          emergency_contact: $scope.emergencyContact
        }, function() {
          $location.path('/emergency_contacts');
        });
      };
    }
  ]);
