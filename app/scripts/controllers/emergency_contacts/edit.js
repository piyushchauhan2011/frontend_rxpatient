'use strict';

angular.module('frontendRxpatientApp')
  .controller('EmergencyContactsEditCtrl', ['$scope', '$rootScope', '$window', '$cookieStore', '$routeParams', '$http', 'EmergencyContacts',
    function($scope, $rootScope, $window, $cookieStore, $routeParams, $http, EmergencyContacts) {
      // var jQuery = $window.jQuery;
      var emergencyContactId = $routeParams.id;
      $scope.emergencyContactId = emergencyContactId;
      $scope.activeEmergencyContacts = 'rx-active';
      $rootScope.toBeShown = true;
      $rootScope.subNavigationShow = true;

      var patientToken = $cookieStore.get('patientToken');
      // var patientId = $cookieStore.get('patientId');

      EmergencyContacts.get({
        id: emergencyContactId,
        token: patientToken
      }, function(emergencyContact) {
        $scope.emergencyContact = emergencyContact;
      });

      $scope.updateEmergencyContact = function() {
        $scope.isDisabled = true;
        delete $scope.emergencyContact.created_at;
        delete $scope.emergencyContact.updated_at;

        EmergencyContacts.update({
          Id: emergencyContactId,
          token: patientToken,
          emergency_contact: $scope.emergencyContact
        }, function() {
          // console.log(message);
          $scope.isDisabled = false;
        }, function() {
          // console.log(err);
          $scope.isDisabled = false;
        });
      };
    }
  ]);
