'use strict';

angular.module('frontendRxpatientApp')
  .controller('SurgeriesEditCtrl', ['$scope', '$rootScope', '$window', '$cookieStore', '$routeParams', '$http', 'Surgeries',
    function($scope, $rootScope, $window, $cookieStore, $routeParams, $http, Surgeries) {
      // var jQuery = $window.jQuery;
      var surgeryId = $routeParams.id;
      $scope.surgeryId = surgeryId;
      $scope.activeSurgeries = 'rx-active';
      $rootScope.toBeShown = true;
      $rootScope.subNavigationShow = true;

      var patientToken = $cookieStore.get('patientToken');
      // var patientId = $cookieStore.get('patientId');

      Surgeries.get({
        id: surgeryId,
        token: patientToken
      }, function(surgery) {
        $scope.surgery = surgery;
      });

      $scope.updateSurgery = function() {
        $scope.isDisabled = true;
        delete $scope.surgery.created_at;
        delete $scope.surgery.updated_at;

        Surgeries.update({
          Id: surgeryId,
          token: patientToken,
          surgery: $scope.surgery
        }, function() {
          // console.log(message);
          $scope.isDisabled = false;
        }, function() {
          // console.log(err);
          $scope.isDisabled = false;
        });
      };
    }
  ]);
