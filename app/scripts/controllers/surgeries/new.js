'use strict';

angular.module('frontendRxpatientApp')
  .controller('SurgeriesNewCtrl', ['$scope', '$rootScope', '$window', '$location', '$cookieStore', '$routeParams', '$http', 'Surgeries',
    function($scope, $rootScope, $window, $location, $cookieStore, $routeParams, $http, Surgeries) {
      // var jQuery = $window.jQuery;
      $scope.activeSurgeries = 'rx-active';
      $rootScope.toBeShown = true;
      $rootScope.subNavigationShow = true;

      var patientToken = $cookieStore.get('patientToken');
      // var patientId = $cookieStore.get('patientId');

      $scope.createSurgery = function() {
        Surgeries.save({
          token: patientToken,
          surgery: $scope.surgery
        }, function() {
          $location.path('/surgeries');
        });
      };
    }
  ]);
