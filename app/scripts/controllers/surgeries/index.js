'use strict';

angular.module('frontendRxpatientApp')
  .controller('SurgeriesIndexCtrl', ['$scope', '$rootScope', '$window', '$location', '$cookieStore', '$routeParams', '$http', 'Surgeries',
    function($scope, $rootScope, $window, $location, $cookieStore, $routeParams, $http, Surgeries) {
      // var jQuery = $window.jQuery;
      
      $scope.activeSurgeries = 'rx-active';
      $rootScope.toBeShown = true;
      $rootScope.subNavigationShow = true;

      var patientToken = $cookieStore.get('patientToken');
      // var patientId = $cookieStore.get('patientId');

      Surgeries.query({
        token: patientToken
      }, function(surgeries) {
        $scope.surgeries = surgeries;
      });

      $scope.navigateToSurgery = function(id) {
        $location.path('/surgeries/edit/'+id);
      };

      $scope.addNewSurgery = function() {
        $location.path('/surgeries/new');
      };
    }
  ]);