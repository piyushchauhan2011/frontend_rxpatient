'use strict';

angular.module('frontendRxpatientApp')
  .controller('ConditionsEditCtrl', ['$scope', '$rootScope', '$window', '$cookieStore', '$routeParams', '$http', 'Conditions',
    function($scope, $rootScope, $window, $cookieStore, $routeParams, $http, Conditions) {
      var jQuery = $window.jQuery;
      var conditionId = $routeParams.id;
      $scope.activeConditions = 'rx-active';
      $rootScope.toBeShown = true;
      $rootScope.subNavigationShow = true;

      var patientToken = $cookieStore.get('patientToken');
      // var patientId = $cookieStore.get('patientId');

      $scope.ptempKey = [];
      $scope.ptempValue = [];
      Conditions.get({
        id: conditionId,
        token: patientToken
      }, function(condition) {
        // var temp = conditions;
        $scope.condition = condition;
        jQuery.each($scope.condition.parameters, function(key, value) {
          // console.log(key + ' ' + value);
          $scope.ptempKey.push(key);
          $scope.ptempValue.push(value);
        });
        // temp[0].parameters = { 'feeling': 'hello world', 'psych-level': 50};
        // console.log(temp);
      });
      $scope.predicate = 'status';
      $scope.parameterOptions = ['a', 'b', 'c', 'd'];
      // $scope.assignParametersToCondition = function(type) {
      //   $scope.parameters = {}
      //   for(var i=0;i<$scope.ptempKey.length;i++) {
      //     $scope.parameters[$scope.ptempKey[i]] = $scope.ptempValue[i];
      //   }
      // }
      $scope.deleteParameter = function(position) {
        $scope.ptempKey.splice(position,1);
        $scope.ptempValue.splice(position,1);
        // console.log($scope.ptempKey.length);
        // console.log($scope.ptempValue);
      };

      $scope.conditionUpdate = function() {
        $scope.condition.parameters = {};
        for(var i=0;i<$scope.ptempKey.length;i++) {
          $scope.condition.parameters[$scope.ptempKey[i]] = $scope.ptempValue[i];
        }
        delete $scope.condition.id;
        delete $scope.condition.created_at;
        delete $scope.condition.updated_at;
        // console.log($scope.ptempKey);
        // console.log($scope.ptempValue);
        // console.log($scope.condition.parameters);
        // console.log($scope.condition);

        Conditions.update({
          Id: conditionId,
          token: patientToken,
          condition: $scope.condition
        }, function() {
          // console.log(message);
        });
        // $scope.condition.save({id: conditionId, token: patientToken}, function(message) { console.log(message); });
      };
    }
  ]);
