'use strict';

angular.module('frontendRxpatientApp')
  .controller('AllergiesNewCtrl', ['$scope', '$rootScope', '$window', '$location', '$cookieStore', '$routeParams', '$http', 'Allergies',
    function($scope, $rootScope, $window, $location, $cookieStore, $routeParams, $http, Allergies) {
      // var jQuery = $window.jQuery;
      // var allergyId = '9a61be15-8e60-482d-9f1c-1accacdb00c9';
      $scope.activeAllergies = 'rx-active';
      $rootScope.toBeShown = true;
      $rootScope.subNavigationShow = true;

      var patientToken = $cookieStore.get('patientToken');
      // var patientId = $cookieStore.get('patientId');

      $scope.createAllergy = function() {
        // console.log($scope.allergy);
        Allergies.save({
          token: patientToken,
          allergy: $scope.allergy
        }, function() {
          // console.log(message);
          $location.path('/allergies');
        });
      };
    }
  ]);
