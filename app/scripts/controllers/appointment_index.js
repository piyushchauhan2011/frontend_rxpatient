'use strict';

angular.module('frontendRxpatientApp')
  .controller('AppointmentIndexCtrl', ['$scope', '$rootScope', function ($scope, $rootScope) {
    $scope.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];

    $rootScope.toBeShown = false;
  }]);
