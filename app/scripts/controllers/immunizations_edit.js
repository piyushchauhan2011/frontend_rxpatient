'use strict';

angular.module('frontendRxpatientApp')
  .controller('ImmunizationsEditCtrl', ['$scope', '$rootScope', '$window', '$location', '$cookieStore', '$routeParams', '$http', 'Immunizations',
    function($scope, $rootScope, $window, $location, $cookieStore, $routeParams, $http, Immunizations) {
      // var jQuery = $window.jQuery;
      // var immunizationsId = '9a61be15-8e60-482d-9f1c-1accacdb00c9';
      var immunizationsId = $routeParams.id;
      $scope.immunizationsId = immunizationsId;
      $scope.activeImmunizations = 'rx-active';
      $rootScope.toBeShown = true;
      $rootScope.subNavigationShow = true;

      var patientToken = $cookieStore.get('patientToken');
      // var patientId = $cookieStore.get('patientId');

      Immunizations.get({
        id: immunizationsId,
        token: patientToken
      }, function(immunization) {
        $scope.immunization = immunization;
      });

      $scope.updateImmunization = function() {
        delete $scope.immunization.created_at;
        delete $scope.immunization.updated_at;

        Immunizations.update({
          Id: immunizationsId,
          token: patientToken,
          immunization_history: $scope.immunization
        }, function() {
          // console.log(message);
          $location.path('/immunizations');
        });
      };
    }
  ]);
