'use strict';

angular.module('frontendRxpatientApp')
  .controller('MedicationNewCtrl', ['$scope', '$rootScope', '$window', '$location', '$cookieStore', '$routeParams', '$http', 'Prescriptions',
    function($scope, $rootScope, $window, $location, $cookieStore, $routeParams, $http, Prescriptions) {
      // var jQuery = $window.jQuery;
      // var prescriptionId = '5920aff3-3936-4130-be9d-aa8b7be39344';
      // var Math = $window.Math;
      // console.log(Math.uuid().toLowerCase());
      var appointmentId = $routeParams.appointmentId;
      $scope.appointmentId = appointmentId;
      $scope.activePrescriptions = 'rx-active';
      $rootScope.toBeShown = true;
      $rootScope.subNavigationShow = true;

      var patientToken = $cookieStore.get('patientToken');
      $scope.medication = {};
      $scope.medication.appointment_id = appointmentId;
      $scope.medication.frequency = [false,false,false,false];
      // var patientId = $cookieStore.get('patientId');

      // Prescriptions.get({
      //   id: medicationId,
      //   token: patientToken
      // }, function(medication) {
      //   $scope.medication = medication;
      // });
      $scope.saveMedication = function() {
        // console.log($scope.medication);
        Prescriptions.save({
          token: patientToken,
          prescription: $scope.medication
        }, function() {
          // console.log(medication);
          $location.path('/prescriptions/view/'+appointmentId);
        });
      };
    }
  ]);
