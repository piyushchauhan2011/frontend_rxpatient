'use strict';

angular.module('frontendRxpatientApp')
  .controller('SavedArticlesCtrl', ['$scope', '$window', '$rootScope', '$cookieStore', '$http', 'Patients', 'Conditions',
    function($scope, $window, $rootScope, $cookieStore, $http, Patients, Conditions) {

      $rootScope.toBeShown = true;
      $rootScope.subNavigationShow = true;
      $rootScope.subNavigationSavedArticles = 'active';

      var patientToken = $cookieStore.get('patientToken');

      // var patientId = $cookieStore.get('patientId');

      $scope.articlesLimit = 10;
      $scope.favourites = true;

      $scope.conditions = Conditions.query({
        token: patientToken
      }, function() {});

      $http.get($rootScope.patientApiUrl+'/articles?token='+patientToken)
      .success(function(articlesFavLikes) {
        $scope.articlesFavLikes = articlesFavLikes;
      });

      $scope.articles = [];
      $scope.getArticle = function(id) {
        $http.get($rootScope.articlesApiUrl+'/articles/get_article/'+id)
        .success(function(article) {
          $scope.articles[id] = article;
        });
      };

      $scope.useful = function(articleFavLikes) {
        if(articleFavLikes.likes === 1) { articleFavLikes.likes = 0; }
        else { articleFavLikes.likes = 1; }
        $http.put($rootScope.patientApiUrl+'/articles/'+articleFavLikes.id+'?token='+patientToken, {
          article: articleFavLikes
        })
        .success(function() {
          // console.log('Successfully Updated');
          // console.log(data);
        })
        .error(function() {
          // console.log(err);
        });
      };

      $scope.notUseful = function(articleFavLikes) {
        if(articleFavLikes.likes === -1) { articleFavLikes.likes = 0; }
        else { articleFavLikes.likes = -1; }
        $http.put($rootScope.patientApiUrl+'/articles/'+articleFavLikes.id+'?token='+patientToken, {
          article: articleFavLikes
        })
        .success(function() {
          // console.log('Successfully Updated');
          // console.log(data);
        })
        .error(function() {
          // console.log(err);
        });
      };
      
      $scope.favourite = function(articleFavLikes) {
        if(articleFavLikes.favourite === true) { articleFavLikes.favourite = false; }
        else { articleFavLikes.favourite = true; }
        $http.put($rootScope.patientApiUrl+'/articles/'+articleFavLikes.id+'?token='+patientToken, {
          article: articleFavLikes
        })
        .success(function() {
          // console.log('Successfully Updated');
          // console.log(data);
        })
        .error(function() {
          // console.log(err);
        });
      };
    }
  ]);
