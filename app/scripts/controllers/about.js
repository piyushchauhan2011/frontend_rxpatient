'use strict';

angular.module('frontendRxpatientApp')
  .controller('AboutCtrl', ['$scope', '$rootScope',
    function($scope, $rootScope) {
      $rootScope.transparentTopBarShow = true;
      $rootScope.topBarShow = false;
      // var patientToken = $cookieStore.get('patientToken');
      // var patientId = $cookieStore.get('patientId');
    }
  ]);
