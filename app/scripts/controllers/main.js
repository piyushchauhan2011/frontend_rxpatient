'use strict';

angular.module('frontendRxpatientApp')
  .controller('MainCtrl', ['$scope', '$rootScope', '$window', '$http', '$cookieStore', '$location', 'Patients',
    function($scope, $rootScope, $window, $http, $cookieStore, $location, Patients) {
      var jQuery = $window.jQuery;
      jQuery('.map-hide').css('display', 'block');
      $rootScope.topBarShow = false;
      $rootScope.transparentTopBarShow = true;
      var hoaxMarginEl = jQuery('#rx_hoax_margin');
      var totalHeight = jQuery(document).height();
      var offsetTop = hoaxMarginEl[0].offsetTop;
      var setNewHeight = totalHeight - offsetTop;
      hoaxMarginEl.css('height', setNewHeight);
      // jQuery('#rx_hoax_margin').hide();

      // $rootScope.bErrors = true;
      // $rootScope.cErrors = 'Successfully Signed Up. Check your email for confirmation.';

      $scope.reset = function() {
        $scope.patient = {};
        // $cookieStore.remove('patientToken');
        // $cookieStore.remove('patientId');
      };

      $scope.login = function(patient) {
        $http.post($rootScope.patientApiUrl +
          '/patients/login',
          patient
        ).success(function(data) {
          var token = data.token;
          $rootScope.patientToken = token;
          // console.log('SUCCESS: TOKEN=' + token);
          $cookieStore.put('patientToken', token);
          $http.get($rootScope.patientApiUrl +
            '/patients/get_id?token=' + token
          ).success(function(data) {
            var id = data.id.id;
            $rootScope.patientId = id;
            // console.log('SUCCESS: ID=' + id);
            $cookieStore.put('patientId', id);
            Patients.get({
              id: id,
              token: token
            }, function(patient) {
              $rootScope.patient = patient;
              $cookieStore.put('patient', patient);
              $rootScope.isUserLoggedIn = true;
              $location.path('/profile');
            }, function() {
              // console.log(err);
            });

          }).error(function() {
            // console.log('ERROR: ' + data);
          });
        }).error(function() {
          // console.log('ERROR: Unauthorized user' + data + 'do some page switch or notification here!');
        });
      };

      $scope.reset();
    }
  ]);
