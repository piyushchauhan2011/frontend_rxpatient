'use strict';

angular.module('frontendRxpatientApp')
  .controller('AppointmentBookCtrl', ['$scope', '$rootScope', '$window', '$cookieStore', 'Appointment', 'Doctors', function ($scope, $rootScope, $window, $cookieStore, Appointment, Doctors) {
    // var moment = $window.moment;
    // var timenow = moment().format('h:mm:ss a');
    $rootScope.toBeShown = false;

    var patientId = $cookieStore.get('patientId');
    // var patientToken = $cookieStore.get('patientToken');

    $scope.doctor = Doctors.get({
      // token: 
    });

    $scope.bookAppointment = function(appointment) {
      var newAppointment = {};
      var today = new Date();
      newAppointment.appointment = appointment;
      newAppointment.appointment.patient_id = patientId;
      newAppointment.appointment.doctor_id = '6f9968d1-8815-4a87-adbe-4b2d2b14f6ee';
      newAppointment.appointment.location_id = 'e7e5b8b1-1ceb-4d0c-9a6a-1eecd28ca0c6';
      newAppointment.appointment.date = today;
      newAppointment.appointment.time = '05:00 pm';
      newAppointment.appointment.status = false;
      // console.log(newAppointment.appointment);
      Appointment.save(newAppointment, function() {
        // console.log(data);
        // var hdate = new Date(data.time);
        // console.log(hdate.getTime());
      });
    };
  }]);
