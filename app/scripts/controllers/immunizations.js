'use strict';

angular.module('frontendRxpatientApp')
  .controller('ImmunizationsCtrl', ['$scope', '$rootScope', '$window', '$location', '$cookieStore', '$routeParams', '$http', 'Immunizations',
    function($scope, $rootScope, $window, $location, $cookieStore, $routeParams, $http, Immunizations) {

      $scope.activeImmunizations = 'rx-active';
      $rootScope.toBeShown = true;
      $rootScope.subNavigationShow = true;

      var patientToken = $cookieStore.get('patientToken');
      // var patientId = $cookieStore.get('patientId');

      Immunizations.query({
        token: patientToken
      }, function(immunizations) {
        $scope.immunizations = immunizations;
      });

      $scope.navigateToImmunization = function(id) {
        $location.path('/immunizations/edit/'+id);
      };

      $scope.addNewImmunization = function() {
        $location.path('/immunizations/new');
      };
    }
  ]);
