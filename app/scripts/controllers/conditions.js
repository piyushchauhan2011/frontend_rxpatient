'use strict';

angular.module('frontendRxpatientApp')
  .controller('ConditionsCtrl', ['$scope', '$rootScope', '$window', '$location', '$cookieStore', '$routeParams', '$http', 'Conditions',
    function($scope, $rootScope, $window, $location, $cookieStore, $routeParams, $http, Conditions) {
      // var jQuery = $window.jQuery;
      $scope.activeConditions = 'rx-active';
      $rootScope.toBeShown = true;
      $rootScope.subNavigationShow = true;

      var patientToken = $cookieStore.get('patientToken');
      // var patientId = $cookieStore.get('patientId');

      Conditions.query({
        token: patientToken
      }, function(conditions) {
        // var temp = conditions;
        $scope.conditions = conditions;
        // temp[0].parameters = { 'feeling': 'hello world', 'psych-level': 50};
        // console.log(temp);
      });
      $scope.predicate = 'status';

      $scope.navigateToCondition = function(id) {
        $location.path('/conditions/edit/'+id);
      };
      $scope.addNewCondition = function() {
        $location.path('/conditions/new');
      };
    }
  ]);
