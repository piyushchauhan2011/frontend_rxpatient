'use strict';

(function( $ ) {

$(document).click(function() {
  $('.rx-log-detail').slideUp('fast');
  $('.rx-btn-detail').removeClass('active');
});

$(document).ready(function() {
  $('.rx-log-detail').click(function() { return false; });
  $('.rx-btn-detail').click(function() {
    $(this).parents('.rx-log').children('.rx-log-detail').slideToggle('fast');
    if (!$(this).hasClass('active')) {
      $(this).addClass('active');
    } else {
      $(this).removeClass('active');
    }
    return false;
  });

  var birthdayVal = $('input[name=' + 'dob]').val();
  $('input[name=' + 'dob]').datepicker({
    changeMonth: true,
    changeYear: true,
    gotoCurrent: true,
    dateFormat: 'MM dd, yy',
    minDate: '-50Y',
    maxDate: '-10Y',
  });
  $('input[name=' + 'dob]').datepicker('option', 'showAnim', 'drop');
  if(birthdayVal!=='') {
    $('input[name=' + 'dob]').datepicker('setDate', new Date(birthdayVal));
  }
  $('.ui-datepicker').wrap('<div id=' + 'rx_gdp></div>');

});

$(document).ready(function() {
  $('#external-events div.external-event').each(function() {
    var eventObject;
    eventObject = {
      title: $.trim($(this).text())
    };
    $(this).data('eventObject', eventObject);
    return $(this).draggable({
      zIndex: 999,
      revert: true,
      revertDuration: 0
    });
  });
  return $('#calendar').fullCalendar({
    header: {
      left: 'prev,next today',
      center: 'title',
      right: 'month,agendaWeek,agendaDay'
    },
    editable: true,
    droppable: true,
    buttonIcons: false,
    buttonText: {
      prev: '<i class='+'fa fa-arrow-circle-left fa-2x'+'></i>',
      next: '<i class='+'fa fa-arrow-circle-right fa-2x'+'></i>'
    },
    theme: true,
    eventClick: function(event) {
      event.title = 'CLICKED!';
      $('#calendar').fullCalendar('updateEvent', event);
      return console.log('Something happened!');
    },
    drop: function(date, allDay) {
      var copiedEventObject, originalEventObject;
      originalEventObject = $(this).data('eventObject');
      copiedEventObject = $.extend({}, originalEventObject);
      copiedEventObject.start = date;
      copiedEventObject.allDay = allDay;
      $('#calendar').fullCalendar('renderEvent', copiedEventObject, true);
      if ($('#drop-remove').is(':checked')) {
        return $(this).remove();
      }
    }
  });
});

})( window.jQuery );