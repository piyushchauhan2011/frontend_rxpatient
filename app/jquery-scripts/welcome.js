'use strict';

(function( $ ) {

$(document).click(function() {
  $('#rx_nav_input_appointment').slideUp('fast');
  // $('.rx-nav-appointment').removeClass('rx-active')
  $('.rx-nav-date').removeClass('rx-active');
});
$(window).resize(function() {
    $('#rx_add_appointment').dialog('option', 'position', 'top');
});
$(document).ready(function() {
  $('.rx-add-appointment').click(function() {
    // $('body').addClass('rx-oh');
    $('#rx_add_appointment').dialog({
      width: 500,
      height: 300,
      modal: true,
      position: {
        my: 'center',
        at: 'top',
        of: window
      },
      show: {
        effect: 'slideDown',
        duration: 200
      },
      hide: {
        effect: 'slideUp',
        duration: 200
      },
      draggable: false,
      close: function() {
        // $('body').removeClass('rx-oh');
      }
    });
  });
  $('#rx_nav_input_appointment').click(function() {
    return false;
  });
  $('#rx_nav_input_appointment').datepicker();
  // $('.rx-nav-appointment').click(function() {
  //   $('#rx_nav_input_appointment').slideToggle('fast');
  //   $(this).toggleClass('rx-active');
  //   return false;
  // });
  $('.rx-nav-date').click(function() {
    $('#rx_nav_input_appointment').slideToggle('fast');
    $(this).toggleClass('rx-active');
    return false;
  });

  $('#date').datepicker({
    changeMonth: true,
    changeYear: true
  });
  $('#dialog').dialog({
    autoOpen: false,
    height: 280,
    modal: true,
    resizable: false,
    buttons: {
      'Continue': function() {
        return $(this).dialog('close');
      },
      'Change Rating': function() {
        return $(this).dialog('close');
      }
    }
  });
  return $('#rating-0').click(function() {
    return $('#dialog').dialog('open');
  });
});
//Abhishek's code
$(document).ready(function() {
  $('.rx-highlight').hover(function() {
    $('#rx_logo').attr('src', '/images/rx-nav-bar-logo-small.png');
  }, function() {
    $('#rx_logo').attr('src', '/images/rx-nav-bar-logo-small-bw.png');
  });
});
//code end
$(document).ready(function() {
  $('#rx_prev').click(function() {
    var el, elprev;
    el = $('#rx_promotion_contents .rx-show');
    elprev = $(el).prev();
    if (elprev.length !== 0) {
      return $(el).hide('drop', {
        direction: 'right'
      }, 300, function() {
        $(el).removeClass('rx-show');
        $(elprev).show('drop', {
          direction: 'left'
        }, 300);
        return $(elprev).addClass('rx-show');
      });
    }
  });
  return $('#rx_next').click(function() {
    var el, elnext;
    el = $('#rx_promotion_contents .rx-show');
    elnext = $(el).next();
    if (elnext.length !== 0) {
      return $(el).hide('drop', {
        direction: 'left'
      }, 300, function() {
        $(el).removeClass('rx-show');
        $(elnext).show('drop', {
          direction: 'right'
        }, 300);
        return $(elnext).addClass('rx-show');
      });
    }
  });
});

})( window.jQuery );