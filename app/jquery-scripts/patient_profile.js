//JavaScript
'use strict';

(function( $ ) {

$(document).ready(function() {
	$('.rx-profile-eb').click(function () {
		if($(this).hasClass('rx-active')) {
			$(this).removeClass('rx-active');
			$(this).html('Edit');
			$('.rx-profile-ec').find('.rx-dm').css('display', 'block');
			$('.rx-profile-ec').find('.rx-em').addClass('rx-dn');
		} else {
			$(this).html('Cancel');
			$(this).addClass('rx-active');
			$('.rx-profile-ec').find('.rx-dm').css('display', 'none');
			$('.rx-profile-ec').find('.rx-em').removeClass('rx-dn');
		}
	});
});

})( window.jQuery );