'use strict';

(function( $ ) {

$(document).ready(function() {
	$('.rx-appsea-rb-d').click(function() { return false; });
  $('.rx-appsea-rb-bab').click(function() {
    $(this).parents('.rx-appsea-rb').children('.rx-appsea-rb-d').slideToggle('fast');
    if (!$(this).children('button').hasClass('active')) {
      $(this).children('button').addClass('active');
    } else {
      $(this).children('button').removeClass('active');
    }
    return false;
  });
});

})( window.jQuery );