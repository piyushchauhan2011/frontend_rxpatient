'use strict';

describe('Controller: AppointmentIndexCtrl', function () {

  // load the controller's module
  beforeEach(module('frontendRxpatientApp'));

  var AppointmentIndexCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    AppointmentIndexCtrl = $controller('AppointmentIndexCtrl', {
      $scope: scope
    });
  }));

  // it('should attach a list of awesomeThings to the scope', function () {
  //   expect(scope.awesomeThings.length).toBe(3);
  // });
});
