'use strict';

describe('Controller: BasicinfoCtrl', function () {

  // load the controller's module
  beforeEach(module('frontendRxpatientApp'));

  var BasicinfoCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    BasicinfoCtrl = $controller('BasicinfoCtrl', {
      $scope: scope
    });
  }));

  // it('should attach a list of awesomeThings to the scope', function () {
  //   expect(scope.awesomeThings.length).toBe(3);
  // });
});
