'use strict';

describe('Controller: AppointmentBookCtrl', function () {

  // load the controller's module
  beforeEach(module('frontendRxpatientApp'));

  var AppointmentBookCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    AppointmentBookCtrl = $controller('AppointmentBookCtrl', {
      $scope: scope
    });
  }));

  // it('should attach a list of awesomeThings to the scope', function () {
  //   expect(scope.awesomeThings.length).toBe(3);
  // });
});
