'use strict';

describe('Controller: VitalsCtrl', function () {

  // load the controller's module
  beforeEach(module('frontendRxpatientApp'));

  var VitalsCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    VitalsCtrl = $controller('VitalsCtrl', {
      $scope: scope
    });
  }));

  // it('should attach a list of awesomeThings to the scope', function () {
  //   expect(scope.awesomeThings.length).toBe(3);
  // });
});
