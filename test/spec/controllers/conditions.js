'use strict';

describe('Controller: ConditionsCtrl', function () {

  // load the controller's module
  beforeEach(module('frontendRxpatientApp'));

  var ConditionsCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    ConditionsCtrl = $controller('ConditionsCtrl', {
      $scope: scope
    });
  }));

  // it('should attach a list of awesomeThings to the scope', function () {
  //   expect(scope.awesomeThings.length).toBe(3);
  // });
});
