'use strict';

describe('Controller: QueriesCtrl', function () {

  // load the controller's module
  beforeEach(module('frontendRxpatientApp'));

  var QueriesCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    QueriesCtrl = $controller('QueriesCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
